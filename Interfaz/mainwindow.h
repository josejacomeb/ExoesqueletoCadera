#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <qcustomplot.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void inicializarSerial();
    void interpretarDatos(QString cadenacortada);
    int datoscadena = 0;
    QString cadena = "";
    QString cadenadistancia1 = "";
    QString cadenadistancia2 = "";
    QString cadenabateria = "";
    QString cadenaimu = "";
    QString cadenayaw = "";
    QVector<double> ejex1, ejex2, ejey, angulox1, anguloy1, angulox2, anguloy2, ultimox1, ultimox2, ultimoy1, ultimoy2, lineax1, lineax2, lineay1, lineay2;
    double tiempo = 0.0;
    ~MainWindow();

private slots:
    int leerSerial();
    void on_botonSerial_clicked();

    void on_botonLevantarse_clicked();

    void on_botonSentarse_clicked();

    void on_botonRetro_clicked();

    void on_selectorVelocidad_currentIndexChanged(const QString &arg1);

    void on_botonRetro_2_clicked();

    void on_botonEmergencia_clicked();

private:
    Ui::MainWindow *ui;
    QSerialPort *arduino;
    static const quint16 arduino_vendor_id = 0x2341; //2341 MEGA
    static const quint16 arduino_product_id = 0x0043; //0043 MEGA
    QByteArray serialData;
    bool iniciocadena = false, fincadena = false;

};

#endif // MAINWINDOW_H
