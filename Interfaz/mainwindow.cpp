#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    inicializarSerial();
    ui->angulos->addGraph(); // blue line
    ui->angulos->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->angulos->graph(0)->setName("Giroscopio Izquierda");
    ui->angulos->addGraph(); // red line
    ui->angulos->graph(1)->setPen(QPen(QColor(255, 110, 40)));
    ui->angulos->graph(1)->setName("Giroscopio Derecha");
    ui->angulos->yAxis->setRange(0, 360);
    ui->piernas->addGraph(); // blue line
    ui->piernas->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->piernas->graph(0)->setLineStyle(QCPGraph::lsNone); //No están
    ui->piernas->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross, 4));
    ui->piernas->graph(0)->setName("Pierna Izquierda");
    ui->piernas->addGraph(); // red line
    ui->piernas->graph(1)->setPen(QPen(QColor(255, 110, 40)));
    ui->piernas->graph(1)->setLineStyle(QCPGraph::lsNone); //No están
    ui->piernas->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross, 4));
    ui->piernas->graph(1)->setName("Pierna Derecha");
    ui->piernas->addGraph(); // red line
    ui->piernas->graph(2)->setPen(QPen(QColor(40, 110, 255)));
    ui->piernas->graph(2)->setLineStyle(QCPGraph::lsNone); //No están
    ui->piernas->graph(2)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 12));
    ui->piernas->addGraph(); // red line
    ui->piernas->graph(3)->setPen(QPen(QColor(255, 110, 40)));
    ui->piernas->graph(3)->setLineStyle(QCPGraph::lsNone); //No están
    ui->piernas->graph(3)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 12));
    ui->piernas->addGraph(); // red line
    ui->piernas->graph(4)->setPen(QPen(QColor(40, 110, 255)));
    ui->piernas->graph(4)->setLineStyle(QCPGraph::lsLine); //No están
    ui->piernas->graph(4)->setName("");
    ui->piernas->addGraph(); // red line
    ui->piernas->graph(5)->setPen(QPen(QColor(255, 110, 40)));
    ui->piernas->graph(5)->setLineStyle(QCPGraph::lsLine); //No están
    ui ->piernas->yAxis->setRange(-1.25,1.25);
    ui->piernas->xAxis->setRange(-1.25, 1.25);
    ui->piernas->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    ui->angulos->legend->setVisible(true);
    ui->angulos->legend->setBrush(QColor(255, 255, 255, 150));
    ultimox1.append(0);
    ultimox2.append(0);
    ultimoy1.append(0);
    ultimoy2.append(0);
    lineax1.append(0);
    lineax1.append(0);
    lineax2.append(0);
    lineax2.append(0);
    lineay1.append(0);
    lineay1.append(0);
    lineay2.append(0);
    lineay2.append(0);
    for(int i = 0; i < 10; i++){
        angulox1.append(0);
        angulox2.append(0);
        anguloy1.append(0);
        anguloy2.append(0);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::inicializarSerial(){
    ///Funciones de Arduino
    arduino = new QSerialPort(this);

    bool arduino_is_available = false;
    QString arduino_port_name;
    //
    //  For each available serial port
    foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()){
        //  check if the serialport has both a product identifier and a vendor identifier
        if(serialPortInfo.hasProductIdentifier() && serialPortInfo.hasVendorIdentifier()){
            //  check if the product ID and the vendor ID match those of the arduino uno
            if((serialPortInfo.productIdentifier() == arduino_product_id)
                    && (serialPortInfo.vendorIdentifier() == arduino_vendor_id)){
                arduino_is_available = true; //    arduino uno is available on this port
                arduino_port_name = serialPortInfo.portName();
                qDebug() << arduino_port_name;
            }
        }
    }
    if(arduino_is_available){
        qDebug() << "Encontrado un Arduino!...\n";
        arduino->setPortName(arduino_port_name);
        arduino->open(QSerialPort::ReadWrite);
        arduino->setBaudRate(QSerialPort::Baud115200);
        arduino->setDataBits(QSerialPort::Data8);
        arduino->setFlowControl(QSerialPort::NoFlowControl);
        arduino->setParity(QSerialPort::NoParity);
        arduino->setStopBits(QSerialPort::OneStop);
        QObject::connect(arduino, SIGNAL(readyRead()), this, SLOT(leerSerial()));
        arduino->bytesAvailable();
        qDebug() << arduino->isOpen();
        arduino->readyRead();
        arduino->setDataTerminalReady(false);
        arduino->setDataTerminalReady(true); // Reiniciar el STM
        ui->botonSerial->setEnabled(false);
    }
    else{
        qDebug() << "No se encuentra conectado un Arduino.\n";
        QMessageBox::information(this, "Error puerto serial", "No se puede abrir el puerto serial del Arduino, conecte uno por favor");
        ui->botonSerial->setEnabled(true);

        QWidget::close();

    }

}

int MainWindow::leerSerial(){
    if(!arduino->isOpen())             {
        ui->botonSerial->setEnabled(true);

        QObject::disconnect(arduino, SIGNAL(readyRead()), this, SLOT(leerSerial()));
        return -1; //Error
    }
    serialData = arduino->readAll();

    for(int j=0;j<serialData.size();j++)
    {
        if(iniciocadena){
            if(serialData.at(j)=='}') {
                fincadena=true;
                interpretarDatos(cadena);
            }
            else {
                cadena += QString(serialData.at(j));
            }
        }
        else{
            if(serialData.at(j)=='{')iniciocadena=true;
            cadena = "";
        }
    }
    return 1;
}

void MainWindow::interpretarDatos(QString cadenacortada){
    //qDebug() << "Cadena debug: " << cadenacortada << "\n";
    fincadena = false;
    iniciocadena = false;
    QStringList datos = cadenacortada.split(',');
    for(int i = 0; i < datos.size(); i++){
        if(datos.at(i) == "ai"){
            ejex1.append(datos.at(i+1).toDouble());
            ui->labelMPUIzq->setText("Ángulo MPU Izquierda: " + datos.at(i+1));
        }
        else if(datos.at(i) == "ad"){
            ejex2.append(datos.at(i+1).toDouble());
            ui->labelMPUDer->setText("Ángulo MPU Derecha: " + datos.at(i+1));
        }
        else if(datos.at(i) == "senal"){
            qDebug() << "Llega: " << datos.at(i + 1) << endl;
        }
        else if(datos.at(i) == "alerta"){
            QMessageBox::information(this, "Error prototipo", QString(datos.at(i + 1)));
            qDebug() << datos.at(i + 1) << endl;
        }
        else if(datos.at(i) == "info"){
            ui->informacion->setText(QString(datos.at(i + 1)));
        }
        else if(datos.at(i) == "accionmi"){
           ui->mi->setText(QString(datos.at(i + 1)));
        }
        else if(datos.at(i) == "accionmd"){
           ui->md->setText(QString(datos.at(i + 1)));
        }
        else if(datos.at(i) == "estado"){
           ui->estado->setText(QString(datos.at(i + 1)));
        }
    }
    if(ejex1.size() > ejey.size()){
        ejey.append(tiempo);
        tiempo+=0.1;
    }
    if(ejex1.size() > 1000){
        ejex1.remove(0);
        ejex2.remove(0);
        ejey.remove(0);
    }
    ui -> angulos->xAxis->setRange(ejey.at(0), ejey.at(ejey.size() - 1));
    ui -> angulos->graph(0)->setData(ejey, ejex1);
    ui -> angulos->graph(1)->setData(ejey, ejex2);
    ui -> angulos->replot();
    if(ejey.size() > 10){
        for(int i = 0; i < 10; i++){
            angulox1[i] = qCos(qDegreesToRadians(ejex1.at(ejex1.size() - 1 - i)));
            anguloy1[i] = qSin(qDegreesToRadians(ejex1.at(ejex1.size() - 1 - i)));
            angulox2[i] = qCos(qDegreesToRadians(ejex2.at(ejex2.size() - 1 - i)));
            anguloy2[i] = qSin(qDegreesToRadians(ejex2.at(ejex2.size() - 1 - i)));
            if(i == 9){
                ultimox1[0] = angulox1[i];
                ultimox2[0] = angulox2[i];
                ultimoy1[0] = anguloy1[i];
                ultimoy2[0] = anguloy2[i];
                lineax1[1] = angulox1[i];
                lineax2[1] = angulox2[i];
                lineay1[1] = anguloy1[i];
                lineay2[1] = anguloy2[i];
            }
        }
        ui -> piernas -> graph(0) ->setData(angulox1, anguloy1);
        ui -> piernas -> graph(1) ->setData(angulox2, anguloy2);
        ui -> piernas -> graph(2) ->setData(ultimox1, ultimoy1);
        ui -> piernas -> graph(3) ->setData(ultimox2, ultimoy2);
        ui -> piernas -> graph(4) ->setData(lineax1, lineay1);
        ui -> piernas -> graph(5) ->setData(lineax2, lineay2);
        ui -> piernas ->replot();
    }
    cadena = "";
}

void MainWindow::on_botonSerial_clicked()
{
    inicializarSerial();
}

void MainWindow::on_botonLevantarse_clicked()
{
    if(arduino->isOpen()){
        arduino->write(QByteArray("l"));
    }
    else{
        QMessageBox::information(this, "Error puerto serial", "No está conectado el puerto serial");
    }
}

void MainWindow::on_botonSentarse_clicked()
{
    if(arduino->isOpen()){
        arduino->write(QByteArray("s"));
    }
    else{
        QMessageBox::information(this, "Error puerto serial", "No está conectado el puerto serial");
    }
}

void MainWindow::on_botonRetro_clicked()
{
    if(arduino->isOpen()){
        arduino->write(QByteArray("r"));
    }
    else{
        QMessageBox::information(this, "Error puerto serial", "No está conectado el puerto serial");
    }
}

void MainWindow::on_selectorVelocidad_currentIndexChanged(const QString &arg1)
{
    if(arduino->isOpen()){
        if(arg1  == "Velocidad Alta"){
            arduino->write(QByteArray("w"));
        }
        else if(arg1  == "Velocidad Baja"){
            arduino->write(QByteArray("v"));
        }
    }
    else{
        QMessageBox::information(this, "Error puerto serial", "No está conectado el puerto serial");
    }
}

void MainWindow::on_botonEmergencia_clicked()
{
    if(arduino->isOpen()){
        if(ui->botonEmergencia->text() == "Emergencia"){
            qDebug("emergencia");
            ui->botonEmergencia->setText(QString("Fin Emergencia"));
            arduino->write(QByteArray("e"));
        }
        else{
            qDebug("Fin Emergencia");
            ui->botonEmergencia->setText(QString("Emergencia"));
            arduino->write(QByteArray("f"));
        }
    }
    else{
        QMessageBox::information(this, "Error puerto serial", "No está conectado el puerto serial");
    }

}
