/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include <qcustomplot.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *logoESPE;
    QLabel *label_3;
    QLabel *label_4;
    QPushButton *botonSerial;
    QLabel *logoMCT;
    QLabel *labelMPUIzq;
    QLabel *labelMPUDer;
    QLabel *label_6;
    QLabel *label_5;
    QCustomPlot *angulos;
    QCustomPlot *piernas;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QPushButton *botonLevantarse;
    QLabel *label;
    QComboBox *selectorVelocidad;
    QPushButton *botonSentarse;
    QPushButton *botonRetro;
    QPushButton *botonEmergencia;
    QLabel *informacion;
    QLabel *mi;
    QLabel *md;
    QLabel *estado;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1024, 768);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        logoESPE = new QLabel(centralWidget);
        logoESPE->setObjectName(QStringLiteral("logoESPE"));
        logoESPE->setGeometry(QRect(50, -10, 81, 71));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(200, 0, 591, 41));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(200, 30, 591, 41));
        botonSerial = new QPushButton(centralWidget);
        botonSerial->setObjectName(QStringLiteral("botonSerial"));
        botonSerial->setGeometry(QRect(670, 120, 131, 34));
        logoMCT = new QLabel(centralWidget);
        logoMCT->setObjectName(QStringLiteral("logoMCT"));
        logoMCT->setGeometry(QRect(800, 0, 81, 71));
        labelMPUIzq = new QLabel(centralWidget);
        labelMPUIzq->setObjectName(QStringLiteral("labelMPUIzq"));
        labelMPUIzq->setGeometry(QRect(650, 360, 241, 18));
        labelMPUDer = new QLabel(centralWidget);
        labelMPUDer->setObjectName(QStringLiteral("labelMPUDer"));
        labelMPUDer->setGeometry(QRect(650, 380, 211, 18));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(30, 370, 120, 18));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy);
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(31, 97, 115, 18));
        sizePolicy.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy);
        angulos = new QCustomPlot(centralWidget);
        angulos->setObjectName(QStringLiteral("angulos"));
        angulos->setGeometry(QRect(30, 110, 501, 261));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(angulos->sizePolicy().hasHeightForWidth());
        angulos->setSizePolicy(sizePolicy1);
        piernas = new QCustomPlot(centralWidget);
        piernas->setObjectName(QStringLiteral("piernas"));
        piernas->setGeometry(QRect(30, 390, 330, 330));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(560, 160, 246, 194));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        botonLevantarse = new QPushButton(layoutWidget);
        botonLevantarse->setObjectName(QStringLiteral("botonLevantarse"));

        gridLayout->addWidget(botonLevantarse, 0, 1, 1, 1);

        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        selectorVelocidad = new QComboBox(layoutWidget);
        selectorVelocidad->setObjectName(QStringLiteral("selectorVelocidad"));

        gridLayout->addWidget(selectorVelocidad, 1, 1, 1, 1);

        botonSentarse = new QPushButton(layoutWidget);
        botonSentarse->setObjectName(QStringLiteral("botonSentarse"));

        gridLayout->addWidget(botonSentarse, 2, 1, 1, 1);

        botonRetro = new QPushButton(layoutWidget);
        botonRetro->setObjectName(QStringLiteral("botonRetro"));

        gridLayout->addWidget(botonRetro, 3, 1, 1, 1);

        botonEmergencia = new QPushButton(layoutWidget);
        botonEmergencia->setObjectName(QStringLiteral("botonEmergencia"));

        gridLayout->addWidget(botonEmergencia, 4, 1, 1, 1);

        informacion = new QLabel(centralWidget);
        informacion->setObjectName(QStringLiteral("informacion"));
        informacion->setGeometry(QRect(516, 440, 421, 81));
        mi = new QLabel(centralWidget);
        mi->setObjectName(QStringLiteral("mi"));
        mi->setGeometry(QRect(530, 650, 461, 20));
        md = new QLabel(centralWidget);
        md->setObjectName(QStringLiteral("md"));
        md->setGeometry(QRect(580, 590, 431, 18));
        estado = new QLabel(centralWidget);
        estado->setObjectName(QStringLiteral("estado"));
        estado->setGeometry(QRect(520, 410, 411, 18));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1024, 30));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        logoESPE->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><img src=\":/imagenes/ESPE.jpeg\"/></p></body></html>", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:18pt; font-weight:600;\">Universidad de las Fuerzas Armadas ESPE </span></p></body></html>", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600;\">CONTROL EXOESQUELETO</span></p><p align=\"center\"><br/></p></body></html>", Q_NULLPTR));
        botonSerial->setText(QApplication::translate("MainWindow", "Conectar Serial", Q_NULLPTR));
        logoMCT->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><img src=\":/imagenes/MCT.jpg\"/></p></body></html>", Q_NULLPTR));
        labelMPUIzq->setText(QApplication::translate("MainWindow", "\303\201ngulo MPU Izquierda: ", Q_NULLPTR));
        labelMPUDer->setText(QApplication::translate("MainWindow", "\303\201ngulo MPU Derecha: ", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindow", "Movimiento Piernas", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "Gr\303\241fico Giroscopios", Q_NULLPTR));
        botonLevantarse->setText(QApplication::translate("MainWindow", "Levantarse", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Caminata Velocidad", Q_NULLPTR));
        selectorVelocidad->clear();
        selectorVelocidad->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Reposo", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Velocidad Alta", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Velocidad Baja", Q_NULLPTR)
        );
        botonSentarse->setText(QApplication::translate("MainWindow", "Sentarse", Q_NULLPTR));
        botonRetro->setText(QApplication::translate("MainWindow", "Retro", Q_NULLPTR));
        botonEmergencia->setText(QApplication::translate("MainWindow", "Emergencia", Q_NULLPTR));
        informacion->setText(QString());
        mi->setText(QString());
        md->setText(QString());
        estado->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
