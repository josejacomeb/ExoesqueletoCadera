/**
   Programa de Exoesqueleto de Cadera
   @author Romero Jackeline y Taco Marco
   Control para motores mediante Gyro MPU6050 y Encoders internos
 **/
/***<---CAMBIAR LA NUMERACION DE LOS PINES ANTES DE CORRER--->***/

#define GUANTE1 23 //Pines digitales para utilizar con pulsador
#define GUANTE2 25
#define GUANTE3 27
#define GUANTE4 29 //Guante para caminar para atras
#define PAROEMERGENCIA 5 //Paro de emergencia
bool emergencia = false;

char caracter; //Para guardar lso datos del serial

//#define ANALOG //Comentar para poner en digitales, descomentar para poner analogos
int umbralguante = 400;

void setup() {
  pinMode(PAROEMERGENCIA, INPUT);
  pinMode(GUANTE1, INPUT);
  pinMode(GUANTE2, INPUT);
  pinMode(GUANTE3, INPUT);
  pinMode(GUANTE4, INPUT);
  Serial.begin(38400);
  Serial.println("Inicializando dispositivos I2C...");
  Serial.println("Esperando...");
  delay(1000);
  Serial.println("Iniciando");
}

void loop() {
  if (digitalRead(PAROEMERGENCIA)) { //Guante de pararse
    delay(400);
    if (!digitalRead(PAROEMERGENCIA)) {
      Serial.println("Paro Emergencia");
    }
  }
#ifdef ANALOG
  if (analogRead(GUANTE1) > umbralguante) { //Guante de pararse
    Serial.println("Analogo Guante 1");
  }
  if (analogRead(GUANTE2) > umbralguante) { //Guante caminarse
    Serial.println("Analogo Guante 2");
  }
  if (analogRead(GUANTE3) > umbralguante) { //Guante sentarse
    Serial.println("Analogo Guante 3");
  }
  if (analogRead(GUANTE4) > umbralguante) { //Guante caminar para atras
    Serial.println("Analogo Guante 4");
  }
#else
  if(digitalRead(GUANTE1) == HIGH) { //Guante de pararse
    delay(100);
    if(digitalRead(GUANTE1) == LOW) {
      Serial.println("Guante 1 Digital");
    }
  }
  if (digitalRead(GUANTE2)) { //Guante de pararse
    delay(400);
    if (!digitalRead(GUANTE2)) {
      Serial.println("Guante 2 Digital");
    }
  }
  if (digitalRead(GUANTE3)) { //Guante de pararse
    delay(400);
    if(!digitalRead(GUANTE3)) {
      Serial.println("Guante 3 Digital");
    }
  }
  if (digitalRead(GUANTE4)) { //Guante de pararse
    delay(400);
    if(!digitalRead(GUANTE4)) {
      Serial.println("Guante 4 Digital");
    }
  }
#endif
}


