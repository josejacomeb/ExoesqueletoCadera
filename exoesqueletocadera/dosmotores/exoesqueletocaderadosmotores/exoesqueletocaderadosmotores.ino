/**
   Programa de Exoesqueleto de Cadera
   @author Romero Jackeline y Taco Marco
   Control para motores mediante Gyro MPU6050, motores Maxon
 **/
/***<---CAMBIAR LA NUMERACION DE LOS PINES ANTES DE CORRER--->***/
#include <Wire.h>  //Comunicacion I2C para acelerometros
const int MPU_Der = 0x69; // Direccion I2C motor enceradoderecho
const int MPU_Izq = 0x68; // Direccion I2C motor enceradoderecho,

int minVal = 265;
int maxVal = 402;
//Angulos para las distintas posiciones
int angulooscilacion = 15;
int anguloparado = 270;
int angulosentado = 350;

double x;
double y;
double angulozderecho = 0;
double angulozizquierdo = 0;

//Ratios de conversion
#define A_R 16384.0
#define G_R 131.0
//Conversion de radianes a grados 180/PI
#define RAD_A_DEG = 57.295779

//MPU-6050 da los valores en enteros de 16 bits
//Valores sin refinar
int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ; //Guardar las aceleraciones y los giros Derecha

//Angulos
float Acc[2];
float Gy[2];
float Angle[2];

bool TERMINAL = false;

#define DEBUG //Descomentar para mostrar datos en el terminal
#define MDCW 9 //Pin para habilitacion y sentido de giro contra sentido manecillas del reloj motor derecho
#define MDCCW 8 //Pin para habilitacion y sentido de giro contra sentido manecillas del reloj motor derecho
#define MDP 10
#define MDPWM 11  //Pin de control PWM
#define MICW 3 //Pin para habilitacion y sentido de giro manecillas reloj motor izquierdo
#define MICCW 2//Pin para habilitacion y sentido de giro contra sentido manecillas del reloj motor izquierdo
#define MIP 4 //Parada pin externo
#define MIPWM 5 //Pin de Control PWM

#define GUANTEPARARSE 23 //Pines digitales para utilizar con pulsador
#define GUANTECAMINAR 25
#define GUANTESENTARSE 27
#define GUANTEPARAATRAS 29 //Guante para caminar para atras
#define controlvelocidad 0
#define PAROEMERGENCIA 20 //Paro de emergencia

bool emergencia = false;

char caracter; //Para guardar lso datos del serial

bool parado = false, caminando = false, sentado = true;
bool enceradoizquierdo = false, enceradoderecho = false; //Variables para verificar el encerado izquierdo y derecho
bool paradoizquierdo = false, paradoderecho = false;     //Vaariables para verificar la posicion del rotor cuando se manda a pararse
bool caminadoizquierdo = false, caminadoderecho = false;     //Vaariables para verificar la posicion del rotor en la posicion de caminar

bool adelanteizquierda = false, adelantederecha = false;
bool pararizquierda = false, pararderecha = false;
int offset = 5;      //Offset de 3º para el control, es dificil que se pare exactamente es por eso del offset
int umbralguante = 200;

void encerar(); //Funcion para encerar los dos motores
void pararse(); //Funcion para que se pare el paciente
void caminar(); //Función para que el exoesquelete haga la funcion de caminar
void aceleracion(bool imprimir); //Encontrar los angulos de z del Acelerometro
int velocidadmotor = 120;
int valorsensor = 0, valorsensoranterior = 0;
int xAng = 0, yAng = 0, zAng = 0;
int error = 0;
int estadoanterior = 0;

void setup() {
  Serial.begin(9600);
  pinMode(MDCW,  OUTPUT);
  pinMode(MDCCW, OUTPUT);
  pinMode(MICW, OUTPUT);
  pinMode(MICCW, OUTPUT);
  pinMode(MIP, OUTPUT);
  pinMode(PAROEMERGENCIA, INPUT);
  pinMode(GUANTEPARARSE, INPUT);
  pinMode(GUANTECAMINAR, INPUT);
  pinMode(GUANTESENTARSE, INPUT);
  digitalWrite(MIP, LOW);
  digitalWrite(MDP, LOW);
  digitalWrite(MDCW, LOW);
  digitalWrite(MDCCW, LOW);
  digitalWrite(MICW, LOW);
  digitalWrite(MICCW, LOW);
  Serial.println("Inicializando dispositivos I2C...");
  Wire.begin(); //Inicia como master
  Wire.beginTransmission(MPU_Der);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(false);
  Wire.beginTransmission(MPU_Izq);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);
  Serial.println("Esperando...");
  delay(2000);
  Serial.println("Iniciando");
}

void loop() {
  valorsensor = analogRead(controlvelocidad);
  if (valorsensor != valorsensoranterior) {
    if (valorsensor >= 0 && valorsensor < 200 ) {
      velocidadmotor = 85;
    }
    else if (valorsensor >= 200 && valorsensor < 400) {
      velocidadmotor = 170;
    }
    else if (valorsensor >= 400) {
      velocidadmotor = 255;
    }
  }
  valorsensoranterior = velocidadmotor;
  if (digitalRead(PAROEMERGENCIA)) { //Guante paro de emergencia
    delay(400);
    if (!digitalRead(PAROEMERGENCIA)) {
      if (emergencia) {
        Serial.println("Desactivada emergencia");
        emergencia = false;
      }
      else {
        Serial.println("Activada emergencia");
        emergencia = true;
      }
    }
  }
  if (!emergencia) {
    if (digitalRead(GUANTEPARARSE)) { //Guante de pararse
      delay(400);
      if (!digitalRead(GUANTEPARARSE)) {
        Serial.println("Pulsado motor para pararse");
        if (sentado == true) {
          pararse();
        }
        else {
          Serial.println("Primero siente al prototipo por favor");
        }

      }
    }
    if (digitalRead(GUANTECAMINAR)) { //Guante caminarse
      delay(400);
      if (!digitalRead(GUANTECAMINAR)) {
        Serial.println("Pulsado motor para caminar hacia adelante");
        if (sentado == true) {
          Serial.println("Primero pare al prototipo por favor");
        }
        else {
          caminar(true);
        }
      }
    }
    if (digitalRead(GUANTESENTARSE)) { //Guante sentarse
      delay(400);
      if (!digitalRead(GUANTESENTARSE)) {
        Serial.println("Pulsado motor para sentarse");
        if (caminando == true ) {
          Serial.println("No se puede sentarse, pare el prototipo primero, for principal");
        }
        else {
          encerar();
        }
      }
    }
    if (digitalRead(GUANTEPARAATRAS)) { //Guante caminar para atras
      delay(400);
      if (!digitalRead(GUANTEPARAATRAS)) {
        Serial.println("Pulsado motor para caminar hacia atras");
        if (sentado == true) {
          Serial.println("Primero pare al prototipo por favor");
        }
        else {
          caminar(false);
        }
      }
    }
  }
}

void encerar() {
  pararizquierda = false;
  pararderecha = false;
  enceradoizquierdo = false;
  enceradoderecho = false;
  aceleracion(false);
  Serial.println("*********Encerando*********");
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.println(angulozderecho);
  digitalWrite(MIP, LOW);
  digitalWrite(MDP, LOW);
  if (angulozizquierdo >= 0 && angulozizquierdo < 180 ) {
    if (angulosentado >= 0  && angulosentado <= 180) {
      if ((angulosentado + offset) >= angulozizquierdo && (angulosentado - offset) <= angulozizquierdo) {
        enceradoizquierdo = true;
      }
      else enceradoizquierdo = false;
    }
    else if (angulosentado > 180  && angulosentado <= 360) {
      if ((angulosentado + offset - 360) >= angulozizquierdo && (angulosentado - offset - 360) <= angulozizquierdo) {
        enceradoizquierdo = true;
      }
      else enceradoizquierdo = false;
    }
  }
  else if (angulozizquierdo >= 180 && angulozizquierdo <= 360) {
    if (angulosentado >= 0  && angulosentado <= 180) {
      if ((angulosentado + offset) >= (angulozizquierdo - 360) && (angulosentado - offset) <= (angulozizquierdo - 360)) {
        enceradoizquierdo = true;
      }
      else enceradoizquierdo = false;
    }
    else if (angulosentado > 180  && angulosentado <= 360) {
      if ((angulosentado + offset) >= angulozizquierdo && (angulosentado - offset) <= angulozizquierdo) {
        enceradoizquierdo = true;
      }
      else enceradoizquierdo = false;
    }
  }
  if (angulozderecho >= 0 && angulozderecho < 180 ) {
    if (angulosentado >= 0  && angulosentado <= 180) {
      if ((angulosentado + offset) >= angulozderecho && (angulosentado - offset) <= angulozderecho) {
        enceradoderecho = true;
      }
      else enceradoderecho = false;
    }
    else if (angulosentado > 180  && angulosentado <= 360) {
      if ((angulosentado + offset - 360) >= angulozderecho && (angulosentado - offset - 360) <= angulozderecho) {
        enceradoderecho = true;
      }
      else enceradoderecho = false;
    }
  }
  else if (angulozderecho >= 180 && angulozderecho <= 360) {
    if (angulosentado >= 0  && angulosentado <= 180) {
      if ((angulosentado + offset) >= (angulozderecho - 360) && (angulosentado - offset) <= (angulozderecho - 360)) {
        enceradoderecho = true;
      }
      else enceradoderecho = false;
    }
    else if (angulosentado > 180  && angulosentado <= 360) {
      if ((angulosentado + offset) >= angulozderecho && (angulosentado - offset) <= angulozderecho) {
        enceradoderecho = true;
      }
      else enceradoderecho = false;
    }
  }
  if (enceradoizquierdo) {
    Serial.println("Rotor izquierdo en sitio adecuado");
    pararizquierda = true;
  }
  else {
    if (angulozizquierdo > 180  && angulozizquierdo <= 360 ) {
      if (angulosentado > 180  && angulosentado <= 360 ) {
        if ((angulosentado - angulozizquierdo) < 0) {
          adelanteizquierda = false;
          Serial.println("Mover rotor izquierdo hacia atras");
        }
        else {
          adelanteizquierda = true;
          Serial.println("Mover rotor izquierdo hacia adelante");
        }
      }
      else if (angulosentado >= 0  && angulosentado <= 180 ) {
        if ((angulosentado + 360 - angulozizquierdo) > 0) {
          adelanteizquierda = false;
          Serial.println("Mover rotor izquierdo hacia atras");
        }
      }
    }
    else if (angulozizquierdo >= 0  && angulozizquierdo <= 180 ) {
      if (angulosentado > 180  && angulosentado <= 360 ) {
        adelanteizquierda = false;
        Serial.println("Mover rotor izquierdo hacia atras");
      }
      else if (angulosentado >= 0  && angulosentado <= 180 ) {
        if ((angulosentado - angulozizquierdo) < 0) {
          adelanteizquierda = false;
          Serial.println("Mover rotor izquierdo hacia atras");
        }
        else {
          adelanteizquierda = true;
          Serial.println("Mover rotor izquierdo hacia adelante");
        }
      }
    }
  }
  if (enceradoderecho) {
    Serial.println("Rotor derecha en sitio adecuado");
    pararderecha = true;
  }
  else {
    if (angulozderecho > 180  && angulozderecho <= 360 ) {
      if (angulosentado > 180  && angulosentado <= 360 ) {
        if ((angulosentado - angulozderecho) > 0) {
          adelantederecha = true;
          Serial.println("Mover rotor derecho hacia adelante");
          Serial.print("azd: [180-360]");
          Serial.println("as: [180-360]");
        }
        else {
          adelantederecha = false;
          Serial.println("Mover rotor derecho hacia atras");
          Serial.print("azd: [180-360]");
          Serial.println("as: [180-360]");
        }
      }
      else if (angulosentado >= 0  && angulosentado <= 180 ) {
        adelantederecha = true;
        Serial.println("Mover rotor derecho hacia atras");
        Serial.print("azd: [180-360]");
        Serial.println("as: [0-180]");
      }
    }
    else if (angulozderecho >= 0  && angulozderecho <= 180 ) {
      if (angulosentado > 180  & angulosentado <= 360 ) {
        adelantederecha = false;
        Serial.println("Mover rotor derecho hacia atras");
        Serial.print("azd: [0-180]");
        Serial.println("as: [180-360]");
      }
      else if (angulosentado >= 0  && angulosentado <= 180 ) {
        if ((angulosentado - angulozderecho) > 0) {
          adelantederecha = true;
          Serial.println("Mover rotor derecha hacia adelante");
          Serial.print("azd: [0-180]");
          Serial.println("as: [0-180]");
        }
        else {
          adelantederecha = false;
          Serial.println("Mover rotor derecha hacia atras");
        }
      }
    }
  }
  while (!enceradoizquierdo || !enceradoderecho) { //Mientras este en otra posicion los pines
    aceleracion(false);
#ifdef DEBUG
    Serial.print("Anguloz izquierdo: ");
    Serial.print(angulozizquierdo);
    Serial.print("  Anguloz derecho: ");
    Serial.println(angulozderecho);
#endif
    if (pararizquierda) {
      Serial.println("Parar izquierda");
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);
      digitalWrite(MIP, HIGH);
      analogWrite(MIPWM, 0);
    }
    else if (adelanteizquierda) {
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, LOW);
      analogWrite(MIPWM, velocidadmotor);
    }
    else if (!adelanteizquierda) {
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, HIGH);
      analogWrite(MIPWM, velocidadmotor);
    }
    if (pararderecha) {
      Serial.println("Parar derecha");
      digitalWrite(MDP, HIGH);
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
      analogWrite(MDPWM, 0);
    }
    else if (adelantederecha) {
      digitalWrite(MDP, LOW);
      digitalWrite(MDCW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, LOW);
      analogWrite(MDPWM, velocidadmotor);
    }
    else if (!adelantederecha) {
      digitalWrite(MDP, LOW);
      digitalWrite(MDCW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, HIGH);
      analogWrite(MDPWM, velocidadmotor);
    }
    if (!enceradoizquierdo) {
      if (angulozizquierdo >= 0 && angulozizquierdo < 180 ) {
        if (angulosentado >= 0  && angulosentado <= 180) {
          if ((angulosentado + offset) >= angulozizquierdo && (angulosentado - offset) <= angulozizquierdo) {
            enceradoizquierdo = true;
          }
          else enceradoizquierdo = false;
        }
        else if (angulosentado > 180  && angulosentado <= 360) {
          if ((angulosentado + offset - 360) >= angulozizquierdo && (angulosentado - offset - 360) <= angulozizquierdo) {
            enceradoizquierdo = true;
          }
          else enceradoizquierdo = false;
        }
      }
      else if (angulozizquierdo >= 180 && angulozizquierdo <= 360) {
        if (angulosentado >= 0  && angulosentado <= 180) {
          if ((angulosentado + offset) >= (angulozizquierdo - 360) && (angulosentado - offset) <= (angulozizquierdo - 360)) {
            enceradoizquierdo = true;
          }
          else enceradoizquierdo = false;
        }
        else if (angulosentado > 180  && angulosentado <= 360) {
          if ((angulosentado + offset) >= angulozizquierdo && (angulosentado - offset) <= angulozizquierdo) {
            enceradoizquierdo = true;
          }
          else enceradoizquierdo = false;
        }
      }
    }
    if (!enceradoderecho) {
      if (angulozderecho >= 0 && angulozderecho < 180 ) {
        if (angulosentado >= 0  && angulosentado <= 180) {
          if ((angulosentado + offset) >= angulozderecho && (angulosentado - offset) <= angulozderecho) {
            enceradoderecho = true;
          }
          else enceradoderecho = false;
        }
        else if (angulosentado > 180  && angulosentado <= 360) {
          if ((angulosentado + offset - 360) >= angulozderecho && (angulosentado - offset - 360) <= angulozderecho) {
            enceradoderecho = true;
          }
          else enceradoderecho = false;
        }
      }
      else if (angulozderecho >= 180 && angulozderecho <= 360) {
        if (angulosentado >= 0  && angulosentado <= 180) {
          if ((angulosentado + offset) >= (angulozderecho - 360) && (angulosentado - offset) <= (angulozderecho - 360)) {
            enceradoderecho = true;
          }
          else enceradoderecho = false;
        }
        else if (angulosentado > 180  && angulosentado <= 360) {
          if ((angulosentado + offset) >= angulozderecho && (angulosentado - offset) <= angulozderecho) {
            enceradoderecho = true;
          }
          else enceradoderecho = false;
        }
      }
    }
    if (enceradoizquierdo) {
      pararizquierda = true;
      Serial.println("Parar izquierda");
    }
    if (enceradoderecho) {
      pararderecha = true;
      Serial.println("Parar derecha");
    }
    delay(10);
    Serial.print("Encerado derecho: ");
    Serial.print(enceradoderecho);
    Serial.print(" Encerado izquierdo: ");
    Serial.println(enceradoizquierdo);
    Serial.print(" && ");
    Serial.println(!enceradoizquierdo && !enceradoderecho);
  }
  digitalWrite(MDCW, LOW); //PARA LOS MOTORES
  digitalWrite(MDCCW, LOW);
  digitalWrite(MICW, LOW);
  digitalWrite(MICCW, LOW);
  digitalWrite(MIP, HIGH);
  digitalWrite(MDP, HIGH);
  Serial.println("*****Acabo de encerar****");
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.println(angulozderecho);
  sentado = true;
  delay(10);
}
void pararse() {
  caminando = false;
  pararizquierda = false;
  pararderecha = false;
  sentado = false;
  aceleracion(false);
  Serial.println("*********Parada*********");
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.print(angulozderecho);

  if (angulozizquierdo < anguloparado + offset && angulozizquierdo > anguloparado - offset) {
    paradoizquierdo = true;
  }
  else paradoizquierdo = false;

  if (angulozderecho < anguloparado + offset && angulozderecho > anguloparado - offset) {
    paradoderecho = true;
  }
  else paradoderecho = false;

  if (paradoizquierdo) {
    Serial.println("Rotor izquierdo en sitio adecuado");
    pararizquierda = true;
  }
  else {
    if ((angulozizquierdo > 270 && angulozizquierdo <= 360) | (angulozizquierdo >= 0 && angulozizquierdo <= 90) ) {
      adelanteizquierda = false;
      Serial.println("Mover rotor izquierdo hacia atras");
    }
    else if (angulozizquierdo > 90 && angulozizquierdo <= 270) {
      adelanteizquierda = true;
      Serial.println("Mover rotor izquierdo hacia adelante");
    }
  }
  if (paradoderecho) {
    Serial.println("Rotor derecho en sitio adecuado");
    pararderecha = true;
  }
  else {
    if ((angulozderecho > 270 && angulozderecho <= 360) | (angulozderecho >= 0 && angulozderecho <= 90) ) {
      adelantederecha = false;
      Serial.println("Mover rotor derecho hacia atras");
    }
    else if (angulozderecho > 90 && angulozderecho <= 270) {
      adelantederecha = true;
      Serial.println("Mover rotor derecho hacia adelante");
    }
  }

  while (!paradoizquierdo || !paradoderecho) { //Mientras este en otra posicion los pines
    aceleracion(false);
#ifdef DEBUG
    Serial.print("Anguloz izquierdo: ");
    Serial.print(angulozizquierdo);
    Serial.print("  Anguloz derecho: ");
    Serial.println(angulozderecho);
#endif
    if (pararizquierda) {
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);
      digitalWrite(MIP, HIGH);
      analogWrite(MIPWM, 0);
      delay(100);
    }
    else if (adelanteizquierda) {
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, LOW);
      analogWrite(MIPWM, velocidadmotor);
    }
    else if (!adelanteizquierda) {
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, HIGH);
      analogWrite(MIPWM, velocidadmotor);
    }
    if (pararderecha) {
      digitalWrite(MDP, HIGH);
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
      analogWrite(MDPWM, 0);
    }
    else if (adelantederecha) {
      digitalWrite(MDP, LOW);
      digitalWrite(MDCW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, LOW);
      analogWrite(MDPWM, velocidadmotor);
    }
    else if (!adelantederecha) {
      digitalWrite(MDP, LOW);
      digitalWrite(MDCW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, HIGH);
      analogWrite(MDPWM, velocidadmotor);
    }
    if (!paradoizquierdo) {
      if (angulozizquierdo < anguloparado + offset && angulozizquierdo > anguloparado - offset) {
        paradoizquierdo = true;
      }
      else paradoizquierdo = false;
    }
    if (!paradoderecho) {
      if (angulozderecho < anguloparado + offset && angulozderecho > anguloparado - offset) {
        paradoderecho = true;
      }
      else paradoderecho = false;
    }
    if (paradoizquierdo) pararizquierda = true;
    if (paradoderecho) pararderecha = true;
    delay(10);
  }
  Serial.println("Finalizo la parada");
  digitalWrite(MDCW, LOW); //PARA LOS MOTORES
  digitalWrite(MDCCW, LOW);
  digitalWrite(MICW, LOW);
  digitalWrite(MICCW, LOW);
  digitalWrite(MIP, HIGH);
  digitalWrite(MDP, HIGH);
  parado = true;
  delay(10);
}

void caminar(bool sentido) {
  caminando = true;
  Serial.println("*****Caminando*****");
  pararizquierda = false;
  pararderecha = false;
  parado = false;
  digitalWrite(MIP, LOW);
  bool adelante = true;
  if (sentido) adelante = true;
  else adelante = false;
  aceleracion(false);
#ifdef DEBUG
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.println(angulozderecho);
#endif
  int offset = 3;
  if (adelante) {
    adelanteizquierda = true;
    adelantederecha = false;
  }
  else {
    adelanteizquierda = false;
    adelantederecha = true;
  }
  for (;;) { //Mientras este en otra posicion los pines

    if (TERMINAL) {
      if (Serial.available()) {
        caracter = Serial.read();
        if (caracter == 'p')  {//Pararse
          caminando = false;
          digitalWrite(MDCW, LOW); //PARA LOS MOTORES
          digitalWrite(MDCCW, LOW);
          digitalWrite(MICW, LOW);
          digitalWrite(MICCW, LOW);
          digitalWrite(MIP, HIGH);
          pararse();
          break;
        }
        else {
          Serial.println("Ingrese una opcion valida");
        }
      }
    }
    else {
      if (!digitalRead(GUANTEPARARSE)) { //Guante de pararse
        delay(400);
        if (digitalRead(GUANTEPARARSE)) {
          caminando = false;
          digitalWrite(MDCW, LOW); //PARA LOS MOTORES
          digitalWrite(MDCCW, LOW);
          digitalWrite(MICW, LOW);
          digitalWrite(MICCW, LOW);
          digitalWrite(MIP, HIGH);
          pararse();
          break;
        }
      }
      if (!digitalRead(GUANTESENTARSE)) { //Guante sentarse
        delay(400);
        if (digitalRead(GUANTESENTARSE)) {
          if (caminando == true ) {
            Serial.println("No se puede sentarse, pare el prototipo primero, for caminar");
          }
          else {
            encerar();
            break;
          }
        }
      }
      if (!digitalRead(GUANTEPARAATRAS)) { //Guante caminar para atras
        delay(400);
        if (digitalRead(GUANTEPARAATRAS)) {
          if (sentado == true) {
            Serial.println("Primero pare al prototipo por favor");
          }
          else {
            caminar(false);
          }
        }
      }
    }
    aceleracion(false);
#ifdef DEBUG
    Serial.print("Anguloz izquierdo: ");
    Serial.print(angulozizquierdo);
    Serial.print("  Anguloz derecho: ");
    Serial.println(angulozderecho);
#endif
    if (pararizquierda) {
      analogWrite(MIPWM, 0);
      Serial.println("Paro");
      digitalWrite(MIP, HIGH);
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);
    }
    else if (adelanteizquierda) {
      analogWrite(MIPWM, velocidadmotor);
      Serial.println("Adelante izquierda");
      digitalWrite(MICW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, LOW);
      digitalWrite(MIP, LOW);
    }
    else if (!adelanteizquierda) {
      analogWrite(MIPWM, velocidadmotor);
      Serial.println("Atras izquierda");
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, HIGH);
    }
    if (pararderecha) {
      analogWrite(MDPWM, 0);
      digitalWrite(MDP, HIGH);
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
    }
    else if (adelantederecha) {
      digitalWrite(MDP, LOW);
      digitalWrite(MDCW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, LOW);
      analogWrite(MDPWM, velocidadmotor);
    }
    else if (!adelantederecha) {
      analogWrite(MDPWM, velocidadmotor);
      digitalWrite(MDP, LOW);
      digitalWrite(MDCW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, HIGH);
    }

    if (angulozizquierdo >= anguloparado + angulooscilacion && adelante) {
      caminadoizquierdo = true;
      pararizquierda = true;
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);
    }
    else if (angulozizquierdo <= anguloparado - angulooscilacion && !adelante) {
      caminadoizquierdo = true;
      pararizquierda = true;
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);

    }
    if (angulozderecho >= anguloparado + angulooscilacion && !adelante) {
      caminadoderecho = true;
      pararderecha = true;
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
    }
    else if (angulozderecho <= anguloparado - angulooscilacion && adelante) {
      caminadoderecho = true;
      pararderecha = true;
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
    }
    if (caminadoizquierdo && caminadoderecho) {
      if (adelante) adelante = false;
      else adelante = true;
      caminadoizquierdo = false;
      caminadoderecho = false;
      pararderecha = false;
      pararizquierda = false;
    }
    if (adelante) {
      adelanteizquierda = true;
      adelantederecha = false;
    }
    else {
      adelanteizquierda = false;
      adelantederecha = true;
    }
    delay(10);
  }
  digitalWrite(MDCW, LOW); //PARA LOS MOTORES
  digitalWrite(MDCCW, LOW);
  digitalWrite(MICW, LOW);
  digitalWrite(MICCW, LOW);
  digitalWrite(MDP, HIGH);
  digitalWrite(MIP, HIGH);
  Serial.println("*****Acabo de caminar*****");
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.print(angulozderecho);

  delay(10);
}

void aceleracion(bool imprimir) {
  //Leer los valores del Acelerometro de la IMU
  Wire.beginTransmission(MPU_Der);
  Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
  error = Wire.endTransmission(false);
  if (error > 0) {
    Serial.print("Error de conexion con el dispositivo izquierdo");
    Serial.print(" Codigo error: ");
    if (error == 1) {
      Serial.println("Dato muy largo");
    }
    else if (error == 2) {
      Serial.println("NACK  transmit of address ");
    }
    else if (error == 3) {
      Serial.println("NACK on transmit of data");
    }
    else if (error == 4) {
      Serial.println("other error ");
    }
  }
  Wire.requestFrom(MPU_Der, 6, true); //A partir del 0x3B, se piden 6 registros
  AcX = Wire.read() << 8 | Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
  AcY = Wire.read() << 8 | Wire.read(); // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ = Wire.read() << 8 | Wire.read(); // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)

  xAng = map(AcX, minVal, maxVal, -90, 90);
  yAng = map(AcY, minVal, maxVal, -90, 90);
  zAng = map(AcZ, minVal, maxVal, -90, 90);

  x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
  y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
  angulozderecho = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
  while ((int)angulozderecho == 225) {
    Serial.println("Error de medida derecho, midiendo de nuevo");
    Wire.beginTransmission(MPU_Der);
    Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
    error = Wire.endTransmission(false);
    if (error > 0) {
      Serial.print("Error de conexion con el dispositivo izquierdo");
      Serial.print(" Codigo error: ");
      if (error == 1) {
        Serial.println("Dato muy largo");
      }
      else if (error == 2) {
        Serial.println("NACK  transmit of address ");
      }
      else if (error == 3) {
        Serial.println("NACK on transmit of data");
      }
      else if (error == 4) {
        Serial.println("other error ");
      }
    }
    Wire.requestFrom(MPU_Der, 6, true); //A partir del 0x3B, se piden 6 registros
    AcX = Wire.read() << 8 | Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
    AcY = Wire.read() << 8 | Wire.read(); // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
    AcZ = Wire.read() << 8 | Wire.read(); // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)

    xAng = map(AcX, minVal, maxVal, -90, 90);
    yAng = map(AcY, minVal, maxVal, -90, 90);
    zAng = map(AcZ, minVal, maxVal, -90, 90);

    x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
    y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
    angulozderecho = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
  }

  //Leer los valores del Acelerometro de la IMU
  Wire.beginTransmission(MPU_Izq);
  Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
  Wire.requestFrom(MPU_Izq, 6, true); //A partir del 0x3B, se piden 6 registros
  AcX = Wire.read() << 8 | Wire.read(); //Cada valor ocupa 2 registros
  AcY = Wire.read() << 8 | Wire.read();
  AcZ = Wire.read() << 8 | Wire.read();

  xAng = map(AcX, minVal, maxVal, -90, 90);
  yAng = map(AcY, minVal, maxVal, -90, 90);
  zAng = map(AcZ, minVal, maxVal, -90, 90);

  x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
  y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
  angulozizquierdo = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
  while ((int)angulozizquierdo == 225) {
    Serial.println("Error de medida izquierdo, midiendo de nuevo");
    Wire.beginTransmission(MPU_Izq);
    Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
    error = (int) Wire.endTransmission(false);
    if (error > 0) {
      Serial.print("Error de conexion con el dispositivo derecho");
      Serial.print("Codigo error: ");
      if (error == 1) {
        Serial.println("Dato muy largo");
      }
      else if (error == 2) {
        Serial.println("NACK transmit of address");
      }
      else if (error == 3) {
        Serial.println("NACK transmit of data");
      }
      else if (error == 4) {
        Serial.println("other error ");
      }
    }
    Wire.requestFrom(MPU_Izq, 6, true); //A partir del 0x3B, se piden 6 registros
    AcX = Wire.read() << 8 | Wire.read(); //Cada valor ocupa 2 registros
    AcY = Wire.read() << 8 | Wire.read();
    AcZ = Wire.read() << 8 | Wire.read();

    xAng = map(AcX, minVal, maxVal, -90, 90);
    yAng = map(AcY, minVal, maxVal, -90, 90);
    zAng = map(AcZ, minVal, maxVal, -90, 90);

    x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
    y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
    angulozizquierdo = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
  }

  if (imprimir) {
    Serial.print("Angulo z izquierdo: ");
    Serial.print(angulozizquierdo);
    Serial.print("Angulo z derecho: ");
    Serial.println(angulozderecho);
  }
}
