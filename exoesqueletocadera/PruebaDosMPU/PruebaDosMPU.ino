/**
   Programa de Exoesqueleto de Cadera
   @author Romero Jackeline y Taco Marco
   Control para motores mediante Gyro MPU6050 y Encoders internos
 **/
/***<---CAMBIAR LA NUMERACION DE LOS PINES ANTES DE CORRER--->***/
#include <Wire.h>  //Comunicacion I2C para acelerometros
const int MPU_Der = 0x69; // Direccion I2C motor enceradoderecho
const int MPU_Izq = 0x68; // Direccion I2C motor enceradoderecho,

const int minVal = 265;
const int maxVal = 402;
//Angulos para las distintas posiciones
int angulooscilacion = 15;
int anguloparado = 270;
int angulosentado = 0;

double x;
double y;
double angulozderecho = 0;
double angulozizquierdo = 0;

//Ratios de conversion
#define A_R 16384.0
#define G_R 131.0
//Conversion de radianes a grados 180/PI
#define RAD_A_DEG = 57.295779

//MPU-6050 da los valores en enteros de 16 bits
//Valores sin refinar
int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ; //Guardar las aceleraciones y los giros Derecha

//Angulos
float Acc[2];
float Gy[2];
float Angle[2];

bool TERMINAL = false;
int xAng = 0, yAng = 0, zAng = 0;
int error = 0;
#define DEBUG //Descomentar para mostrar datos en el terminal

void setup() {
  Serial.begin(115200);
  Serial.println("Inicializando dispositivos I2C...");
  Wire.begin(); //Inicia como master
  Wire.beginTransmission(MPU_Der);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(false);
  Wire.beginTransmission(MPU_Izq);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);
}

void loop() {
  aceleracion(true);
  delay(10);
}

void aceleracion(bool imprimir) {
  //Leer los valores del Acelerometro de la IMU
  Wire.beginTransmission(MPU_Der);
  Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
  error = Wire.endTransmission(false);
  if (error > 0) {
    Serial.print("Error de conexion con el dispositivo izquierdo");
    Serial.print(" Codigo error: ");
    if (error == 1) {
      Serial.println("Dato muy largo");
    }
    else if (error == 2) {
      Serial.println("NACK  transmit of address ");
    }
    else if (error == 3) {
      Serial.println("NACK on transmit of data");
    }
    else if (error == 4) {
      Serial.println("other error ");
    }
  }
  Wire.requestFrom(MPU_Der, 6, true); //A partir del 0x3B, se piden 6 registros
  AcX = Wire.read() << 8 | Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
  AcY = Wire.read() << 8 | Wire.read(); // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ = Wire.read() << 8 | Wire.read(); // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  while (AcX == 0 && AcY == 0 && AcZ == 0) {
    Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
    Wire.requestFrom(MPU_Der, 6, true); //A partir del 0x3B, se piden 6 registros
    AcX = Wire.read() << 8 | Wire.read(); //Cada valor ocupa 2 registros
    AcY = Wire.read() << 8 | Wire.read();
    AcZ = Wire.read() << 8 | Wire.read();
  }

  xAng = map(AcX, minVal, maxVal, -90, 90);
  yAng = map(AcY, minVal, maxVal, -90, 90);
  zAng = map(AcZ, minVal, maxVal, -90, 90);

  x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
  y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
  angulozderecho = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
  while ((int)angulozderecho == 225) {
    Serial.println("Error de medida derecho, midiendo de nuevo");
    Wire.beginTransmission(MPU_Der);
    Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
    error = Wire.endTransmission(false);
    if (error > 0) {
      Serial.print("Error de conexion con el dispositivo izquierdo");
      Serial.print(" Codigo error: ");
      if (error == 1) {
        Serial.println("Dato muy largo");
      }
      else if (error == 2) {
        Serial.println("NACK  transmit of address ");
      }
      else if (error == 3) {
        Serial.println("NACK on transmit of data");
      }
      else if (error == 4) {
        Serial.println("other error ");
      }
    }
    Wire.requestFrom(MPU_Der, 6, true); //A partir del 0x3B, se piden 6 registros
    AcX = Wire.read() << 8 | Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
    AcY = Wire.read() << 8 | Wire.read(); // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
    AcZ = Wire.read() << 8 | Wire.read(); // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)

    xAng = map(AcX, minVal, maxVal, -90, 90);
    yAng = map(AcY, minVal, maxVal, -90, 90);
    zAng = map(AcZ, minVal, maxVal, -90, 90);

    x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
    y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
    angulozderecho = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
  }

  //Leer los valores del Acelerometro de la IMU
  Wire.beginTransmission(MPU_Izq);
  Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
  Wire.requestFrom(MPU_Izq, 6, true); //A partir del 0x3B, se piden 6 registros
  AcX = Wire.read() << 8 | Wire.read(); //Cada valor ocupa 2 registros
  AcY = Wire.read() << 8 | Wire.read();
  AcZ = Wire.read() << 8 | Wire.read();
  //Serial.print("AcX: "); Serial.print(AcX); Serial.print(" Acy: "); Serial.print(AcY); Serial.print(" Acz: "); Serial.println(AcZ);
  while (AcX == 0 && AcY == 0 && AcZ == 0) {
    Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
    Wire.requestFrom(MPU_Izq, 6, true); //A partir del 0x3B, se piden 6 registros
    AcX = Wire.read() << 8 | Wire.read(); //Cada valor ocupa 2 registros
    AcY = Wire.read() << 8 | Wire.read();
    AcZ = Wire.read() << 8 | Wire.read();
  }
  xAng = map(AcX, minVal, maxVal, -90, 90);
  yAng = map(AcY, minVal, maxVal, -90, 90);
  zAng = map(AcZ, minVal, maxVal, -90, 90);

  x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
  y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
  angulozizquierdo = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
  while ((int)angulozizquierdo == 225) {
    Serial.println("Error de medida izquierdo, midiendo de nuevo");
    Wire.beginTransmission(MPU_Izq);
    Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
    error = (int) Wire.endTransmission(false);
    if (error > 0) {
      //Serial.print("Error de conexion con el dispositivo derecho");
      //Serial.print("Codigo error: ");
      if (error == 1) {
        Serial.println("Dato muy largo");
      }
      else if (error == 2) {
        Serial.println("NACK transmit of address");
      }
      else if (error == 3) {
        Serial.println("NACK transmit of data");
      }
      else if (error == 4) {
        Serial.println("other error ");
      }
    }
    Wire.requestFrom(MPU_Izq, 6, true); //A partir del 0x3B, se piden 6 registros
    AcX = Wire.read() << 8 | Wire.read(); //Cada valor ocupa 2 registros
    AcY = Wire.read() << 8 | Wire.read();
    AcZ = Wire.read() << 8 | Wire.read();
    xAng = map(AcX, minVal, maxVal, -90, 90);
    yAng = map(AcY, minVal, maxVal, -90, 90);
    zAng = map(AcZ, minVal, maxVal, -90, 90);

    x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
    y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
    angulozizquierdo = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
  }

  if (imprimir) {
  Serial.print("{ai,");   Serial.print(angulozizquierdo); Serial.print(","); Serial.print("ad,");  Serial.print(angulozderecho); Serial.println("}\0");
  }
}
