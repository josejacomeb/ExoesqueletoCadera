/**
 * Programa de Exoesqueleto de Cadera
 * @author Romero Jackeline y Taco Marco
 * Control para motores mediante Gyro MPU6050 y Encoders internos
 **/
/***<---CAMBIAR LA NUMERACION DE LOS PINES ANTES DE CORRER--->***/
#include <Wire.h>  //Comunicacion I2C para acelerometros
const int MPU_Der=0x69;  // Direccion I2C motor enceradoderecho
const int MPU_Izq=0x68;  // Direccion I2C motor enceradoderecho,

int minVal=265;
int maxVal=402;
//Angulos para las distintas posiciones
int angulooscilacion = 15;
int anguloparado = 270;
int angulosentado = 350;

double x;
double y;
double angulozderecho = 0;
double angulozizquierdo = 0;

//Ratios de conversion
#define A_R 16384.0
#define G_R 131.0
//Conversion de radianes a grados 180/PI
#define RAD_A_DEG = 57.295779

//MPU-6050 da los valores en enteros de 16 bits
//Valores sin refinar
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ; //Guardar las aceleraciones y los giros Derecha

//Angulos
float Acc[2];
float Gy[2];
float Angle[2];

bool TERMINAL = false;

#define DEBUG //Descomentar para mostrar datos en el terminal
#define MDCW 10 //Pin para habilitacion y sentido de giro contra sentido manecillas del reloj motor derecho
#define MDCCW 9 //Pin para habilitacion y sentido de giro contra sentido manecillas del reloj motor derecho
#define MICW 2 //Pin para habilitacion y sentido de giro manecillas reloj motor izquierdo
#define MICCW 3//Pin para habilitacion y sentido de giro contra sentido manecillas del reloj motor izquierdo
#define MIP 5 //Parada pin externo

#define GUANTE1 22 //Pines digitales para utilizar con pulsador
#define GUANTE2 24
#define GUANTE3 26

char caracter;  //Para guardar lso datos del serial

bool parado = false, caminando = false, sentado = true;
bool enceradoizquierdo = false, enceradoderecho = false; //Variables para verificar el encerado izquierdo y derecho
bool paradoizquierdo = false, paradoderecho = false;     //Vaariables para verificar la posicion del rotor cuando se manda a pararse
bool caminadoizquierdo = false, caminadoderecho = false;     //Vaariables para verificar la posicion del rotor en la posicion de caminar

bool adelanteizquierda = false, adelantederecha = false;
bool pararizquierda = false, pararderecha = false;
int offset = 3;      //Offset de 3º para el control, es dificil que se pare exactamente es por eso del offset
int umbralguante = 200;

void encerar(); //Funcion para encerar los dos motores
void pararse(); //Funcion para que se pare el paciente
void caminar(); //Función para que el exoesquelete haga la funcion de caminar
void sentarse(); //Funcion para volver al estado inicial
void aceleracion(bool imprimir); //Encontrar los angulos de z del Acelerometro

void setup(){
  pinMode(MDCW,  OUTPUT);
  pinMode(MDCCW, OUTPUT);
  pinMode(MICW, OUTPUT);
  pinMode(MICCW, OUTPUT);
  pinMode(MIP, OUTPUT);
  pinMode(GUANTE1,INPUT);
  pinMode(GUANTE2, INPUT);
  pinMode(GUANTE3,INPUT);
  digitalWrite(MIP, LOW);
  Serial.begin(38400);
  Serial.println("Inicializando dispositivos I2C...");
  Wire.begin(); //Inicia como master
  Wire.beginTransmission(MPU_Der);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(false);
  Wire.beginTransmission(MPU_Izq);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);
  Serial.println("Esperando...");
  delay(5000);
  Serial.println("Iniciando");
  //encerar();
  if(TERMINAL)  Serial.println("Escribir en el terminal: p (pararse), c(caminar), s (sentarse)");
}

void loop(){
  if(TERMINAL){
    if(Serial.available()){
      caracter = Serial.read();
      if(caracter == 'p')  {  //Pararse
        pararse();
      }
      else if(caracter == 'c'){ //Caminar
        if(sentado == true){
          Serial.println("Primero pare al prototipo por favor");
        }
        else{
          caminar();
        }
      }
      else if(caracter == 's'){
        if(caminando == true ){
          Serial.println("No se puede sentarse, pare el prototipo primero ");
        }
        else{
          encerar();
        }
      }
      else if(caracter == 'g'){
        aceleracion(true);
      }
      else{
        Serial.println("Ingrese una opcion valida");
      }
    }
  }
  else{
    if(!digitalRead(GUANTE1)){ //Guante de pararse
      delay(500);
      if(digitalRead(GUANTE1)){
        pararse();
      }
    }
    if(!digitalRead(GUANTE2)){ //Guante caminarse
      delay(500);
      if(digitalRead(GUANTE2)){
        if(sentado == true){
          Serial.println("Primero pare al prototipo por favor");
        }
        else{
          caminar();
        }
      }
    }
    if(!digitalRead(GUANTE3)){ //Guante sentarse
      delay(500);
      if(digitalRead(GUANTE3)){
        if(caminando == true ){
          Serial.println("No se puede sentarse, pare el prototipo primero ");
        }
        else{
          encerar();
        }
      }
    }
    delay(100);
  }
}

void encerar(){
  pararizquierda = false;
  pararderecha = false;
  aceleracion(false);
  Serial.println("*********Encerando*********");
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.println(angulozderecho);

  if(angulozizquierdo >= 0 & angulozizquierdo < 180 ){
    if(angulosentado >= 0  & angulosentado <= 180){
      int bordeinterno = 0;
      if((angulosentado - offset) < 0 ){
        bordeinterno = 0;
      }
      else bordeinterno = angulosentado - offset;
      if(angulozizquierdo <= angulosentado + offset && angulozizquierdo >= angulosentado - offset ){
        enceradoizquierdo = true;
      }
      else enceradoizquierdo = false;
    }
    else if(angulosentado > 180  & angulosentado <= 360){
      if(angulozizquierdo + 360 <= angulosentado + offset && angulozizquierdo + 360 >= angulosentado - offset ){
        enceradoizquierdo = true;
      }
    }
  }
  else if(angulozizquierdo <= 360 & angulozizquierdo >= 180){
    if(angulosentado >= 0  & angulosentado <= 180){
      if(angulozizquierdo <= angulosentado + 360 + offset & angulozizquierdo >= angulosentado + 360 - offset){
        enceradoizquierdo = true;
      }
      else enceradoizquierdo = false;
    }
    else if(angulosentado > 180  & angulosentado <= 360){
      if(angulozizquierdo <= angulosentado + offset & angulozizquierdo >= angulosentado - offset){
        enceradoizquierdo = true;
      }
      else enceradoizquierdo = false;
    }
  }
  if(angulozderecho >= 0){
    if(angulozderecho < 0.10 + offset && angulozderecho > 0 ){
      enceradoderecho = true;
    }
    else enceradoderecho = false;
  }
  else if(angulozderecho <= 360){
    if(angulozderecho < 360 && angulozderecho > 360 - offset){
      enceradoderecho = true;
    }
    else enceradoderecho = false;
  }
  if(enceradoizquierdo) {
    Serial.println("Rotor izquierdo en sitio adecuado");
    pararizquierda = true;
  }
  else {
    if(angulozizquierdo > 180  & angulozizquierdo <= 360 ){
      if(angulosentado > 180  & angulosentado <= 360 ){
        if((angulosentado - angulozizquierdo) < 0){
          adelanteizquierda = false;
          Serial.println("Mover rotor izquierdo hacia atras");
        }
        else{
          adelanteizquierda = true;
          Serial.println("Mover rotor izquierdo hacia adelante");
        }
      }
      else if(angulosentado >= 0  && angulosentado <= 180 ){
        if((angulosentado + 360 - angulozizquierdo) > 0){
          adelanteizquierda = false;
          Serial.println("Mover rotor izquierdo hacia atras");
        }
      }
    }
    else if(angulozizquierdo >= 0  && angulozizquierdo <= 180 ){
      if(angulosentado > 180  & angulosentado <= 360 ){
        adelanteizquierda = false;
        Serial.println("Mover rotor izquierdo hacia atras");
      }
      else if(angulosentado >= 0  && angulosentado <= 180 ){
        if((angulosentado - angulozizquierdo) < 0){
          adelanteizquierda = false;
          Serial.println("Mover rotor izquierdo hacia atras");
        }
        else{
          adelanteizquierda = true;
          Serial.println("Mover rotor izquierdo hacia adelante");
        }
      }
    }
    if(angulozizquierdo >= 0  && angulozizquierdo <= 180 ){
      adelanteizquierda = false;
      Serial.println("Mover rotor izquierdo hacia atras");
    }
    else if(angulozizquierdo <= 360 && angulozizquierdo > 180){
      adelanteizquierda = true;
      Serial.println("Mover rotor izquierdo hacia adelante");
    }
  }
  if(enceradoderecho) {
    Serial.println("Rotor derecho en sitio adecuado");
    pararderecha = true;
  }
  else {
    if(angulozderecho >= 0 & angulozderecho <= 180 ){
      adelantederecha = false;
      Serial.println("Mover rotor derecho hacia atras");
    }
    else if(angulozderecho <= 360 & angulozderecho > 180){
      adelantederecha = true;
      Serial.println("Mover rotor derecho hacia adelante");
    }
  }
  while(!enceradoizquierdo /*& enceradoderecho*/ ){ //Mientras este en otra posicion los pines
    aceleracion(false);
#ifdef DEBUG
    Serial.print("Anguloz izquierdo: ");
    Serial.print(angulozizquierdo);
    Serial.print("  Anguloz derecho: ");
    Serial.println(angulozderecho);
#endif
   if(pararizquierda){
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);
      digitalWrite(MIP, HIGH);
      delay(100);
    }
    else if(adelanteizquierda){
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, LOW);
    }
    else if(!adelanteizquierda){
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, HIGH);
    }
    if(pararderecha){
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
    }
    else if(adelantederecha){
      digitalWrite(MDCW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, LOW);
    }
    else if(!adelantederecha){
      digitalWrite(MDCW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, HIGH);
    }
    if(angulozizquierdo >= 0 & angulozizquierdo < 180 ){
      if(angulosentado >= 0  & angulosentado <= 180){
        int bordeinterno = 0;
        if((angulosentado - offset) < 0 ){
          bordeinterno = 0;
        }
        else bordeinterno = angulosentado - offset;
        if(angulozizquierdo <= angulosentado + offset && angulozizquierdo >= angulosentado - offset ){
          enceradoizquierdo = true;
        }
        else enceradoizquierdo = false;
      }
      else if(angulosentado > 180  & angulosentado <= 360){
        if(angulozizquierdo + 360 <= angulosentado + offset && angulozizquierdo + 360 >= angulosentado - offset ){
          enceradoizquierdo = true;
        }
      }
    }
    else if(angulozizquierdo <= 360 & angulozizquierdo >= 180){
      if(angulosentado >= 0  & angulosentado <= 180){
        if(angulozizquierdo <= angulosentado + 360 + offset & angulozizquierdo >= angulosentado + 360 - offset){
          enceradoizquierdo = true;
        }
        else enceradoizquierdo = false;
      }
      else if(angulosentado > 180  & angulosentado <= 360){
        if(angulozizquierdo <= angulosentado + offset & angulozizquierdo >= angulosentado - offset){
          enceradoizquierdo = true;
        }
        else enceradoizquierdo = false;
      }
    }
    if(angulozderecho >= 0){
      if(angulozderecho < 0.10 + offset && angulozderecho > 0 ){
        enceradoderecho = true;
      }
      else enceradoderecho = false;
    }
    else if(angulozderecho <= 360){
      if(angulozderecho < 360 && angulozderecho > 360 - offset){
        enceradoderecho = true;
      }
      else enceradoderecho = false;
    }
    if(enceradoizquierdo) pararizquierda = true;
    if(enceradoderecho) pararderecha = true;
    delay(10);
    Serial.flush();
  }
  digitalWrite(MDCW, LOW); //PARA LOS MOTORES
  digitalWrite(MDCCW, LOW);
  digitalWrite(MICW, LOW);
  digitalWrite(MICCW, LOW);
  digitalWrite(MIP, HIGH);
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.println(angulozderecho);
  sentado = true;
  Serial.println("*****Acabo de encerar****");
  delay(10);
}
void pararse(){
  caminando = false;
  pararizquierda = false;
  pararderecha = false;
  sentado = false;
  aceleracion(false);
  Serial.println("*********Parada*********");
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.print(angulozderecho);

  if(angulozizquierdo < anguloparado + offset & angulozizquierdo > anguloparado - offset){
    paradoizquierdo = true;
  }
  else paradoizquierdo = false;

  if(angulozderecho < anguloparado + offset & angulozderecho > anguloparado - offset){
    paradoderecho = true;
  }
  else paradoderecho = false;

  if(paradoizquierdo) {
    Serial.println("Rotor izquierdo en sitio adecuado");
    pararizquierda = true;
  }
  else {
    if((angulozizquierdo > 270 & angulozizquierdo <= 360) | (angulozizquierdo >= 0 & angulozizquierdo <= 90) ){
      adelanteizquierda = false;
      Serial.println("Mover rotor izquierdo hacia atras");
    }
    else if(angulozizquierdo > 90 & angulozizquierdo <= 270){
      adelanteizquierda = true;
      Serial.println("Mover rotor izquierdo hacia adelante");
    }
  }
  if(paradoderecho) {
    Serial.println("Rotor derecho en sitio adecuado");
    pararderecha = true;
  }
  else {
    if((angulozderecho > 270 & angulozderecho <= 360) | (angulozderecho >= 0 & angulozderecho <= 90) ){
      adelantederecha = false;
      Serial.println("Mover rotor derecho hacia atras");
    }
    else if(angulozderecho > 90 & angulozderecho <= 270){
      adelantederecha = true;
      Serial.println("Mover rotor derecho hacia adelante");
    }
  }

  while(!paradoizquierdo /*& paradoderecho*/ ){ //Mientras este en otra posicion los pines
    aceleracion(false);
#ifdef DEBUG
    Serial.print("Anguloz izquierdo: ");
    Serial.print(angulozizquierdo);
    Serial.print("  Anguloz derecho: ");
    Serial.println(angulozderecho);
#endif
    if(pararizquierda){
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);
      digitalWrite(MIP, HIGH);
      delay(100);
    }
    else if(adelanteizquierda){
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, LOW);
    }
    else if(!adelanteizquierda){
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, HIGH);
    }
    if(pararderecha){
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
    }
    else if(adelantederecha){
      digitalWrite(MDCW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, LOW);
    }
    else if(!adelantederecha){
      digitalWrite(MDCW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, HIGH);
    }

    if(angulozizquierdo < anguloparado + offset & angulozizquierdo > anguloparado - offset){
      paradoizquierdo = true;
    }
    else paradoizquierdo = false;

    if(angulozderecho < anguloparado + offset & angulozderecho > anguloparado - offset){
      paradoderecho = true;
    }
    else paradoderecho = false;
    if(paradoizquierdo) pararizquierda = true;
    if(paradoderecho) pararderecha = true;
    delay(10);
  }
  digitalWrite(MDCW, LOW); //PARA LOS MOTORES
  digitalWrite(MDCCW, LOW);
  digitalWrite(MICW, LOW);
  digitalWrite(MICCW, LOW);
  digitalWrite(MIP, HIGH);
  Serial.println("Finalizo la parada");
  parado = true;
  delay(10);
}

void caminar(){
  Serial.println("*****Caminando*****");
  pararizquierda = false;
  pararderecha = false;
  parado = false;
  digitalWrite(MIP, LOW);
  bool adelante = true;
  aceleracion(false);
#ifdef DEBUG
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.println(angulozderecho);
#endif
  int offset = 3;
  if(adelante) {
    adelanteizquierda = true;
    adelantederecha = false;
  }
  else{
    adelanteizquierda = false;
    adelantederecha = true;
  }
  for(;;){ //Mientras este en otra posicion los pines

    if(TERMINAL){
      if(Serial.available()){
        caracter = Serial.read();
        if(caracter == 'p')  {  //Pararse
          caminando = false;
          digitalWrite(MDCW, LOW); //PARA LOS MOTORES
          digitalWrite(MDCCW, LOW);
          digitalWrite(MICW, LOW);
          digitalWrite(MICCW, LOW);
          digitalWrite(MIP, HIGH);
          pararse();
          break;
        }
        else{
          Serial.println("Ingrese una opcion valida");
        }
      }
    }
    else{
      if(!digitalRead(GUANTE1)){ //Guante de pararse
        delay(400);
        if(digitalRead(GUANTE1)){
          caminando = false;
          digitalWrite(MDCW, LOW); //PARA LOS MOTORES
          digitalWrite(MDCCW, LOW);
          digitalWrite(MICW, LOW);
          digitalWrite(MICCW, LOW);
          digitalWrite(MIP, HIGH);
          pararse();
          break;
        }
      }
      if(!digitalRead(GUANTE3)){ //Guante sentarse
        delay(400);
        if(digitalRead(GUANTE3)){
          if(caminando == true ){
            Serial.println("No se puede sentarse, pare el prototipo primero ");
          }
          else{
            encerar();
          }
        }
      }
    }
    aceleracion(false);
#ifdef DEBUG
    Serial.print("Anguloz izquierdo: ");
    Serial.print(angulozizquierdo);
    Serial.print("  Anguloz derecho: ");
    Serial.println(angulozderecho);
#endif
    if(pararizquierda){
      Serial.println("Paro");
      digitalWrite(MIP, HIGH);
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);
    }
    else if(adelanteizquierda){
      Serial.println("Adelante izquierda");
      digitalWrite(MICW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, LOW);
      digitalWrite(MIP, LOW);
    }
    else if(!adelanteizquierda){
      Serial.println("Atras izquierda");
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, HIGH);
    }
    if(pararderecha){
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
    }
    else if(adelantederecha){
      digitalWrite(MDCW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, LOW);
    }
    else if(!adelantederecha){
      digitalWrite(MDCW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, HIGH);
    }

    if(angulozizquierdo >= anguloparado + angulooscilacion & adelante){
      caminadoizquierdo = true;
      pararizquierda = true;
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);
    }
    else if(angulozizquierdo <= anguloparado - angulooscilacion & !adelante){
      caminadoizquierdo = true;
      pararizquierda = true;
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);

    }
    if(angulozderecho >= anguloparado + angulooscilacion & !adelante){
      caminadoderecho = true;
      pararderecha = true;
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
    }
    else if(angulozderecho <= anguloparado - angulooscilacion & adelante){
      caminadoderecho = true;
      pararderecha = true;
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
    }
    if(caminadoizquierdo /*& caminadoderecho*/){
      if(adelante) adelante = false;
      else adelante = true;
      caminadoizquierdo = false;
      caminadoderecho = false;
      pararderecha = false;
      pararizquierda = false;
    }
    if(adelante) {
      adelanteizquierda = true;
      adelantederecha = false;
    }
    else{
      adelanteizquierda = false;
      adelantederecha = true;
    }
    delay(10);
  }
  digitalWrite(MDCW, LOW); //PARA LOS MOTORES
  digitalWrite(MDCCW, LOW);
  digitalWrite(MICW, LOW);
  digitalWrite(MICCW, LOW);
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.print(angulozderecho);
  Serial.println("Acabo de caminar");
  caminando = true;
  delay(10);
}
void aceleracion(bool imprimir){
  //Leer los valores del Acelerometro de la IMU
  Wire.beginTransmission(MPU_Der);
  Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
  int error = Wire.endTransmission(false);
  if(error > 0){
    Serial.print("Error de conexion con el dispositivo izquierdo");
    Serial.print(" Codigo error: ");
    if(error == 1){
      Serial.println("Dato muy largo");
    }
    else if(error == 2){
      Serial.println("NACK  transmit of address ");
    }
    else if(error == 3){
      Serial.println("NACK on transmit of data");
    }
    else if(error == 4){
      Serial.println("other error ");
    }
  }
  Wire.requestFrom(MPU_Der,6,true); //A partir del 0x3B, se piden 6 registros
  AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
  AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)

  int xAng = map(AcX,minVal,maxVal,-90,90);
  int yAng = map(AcY,minVal,maxVal,-90,90);
  int zAng = map(AcZ,minVal,maxVal,-90,90);

  x= RAD_TO_DEG * (atan2(-yAng, -zAng)+PI);
  y= RAD_TO_DEG * (atan2(-xAng, -zAng)+PI);
  angulozderecho = RAD_TO_DEG * (atan2(-yAng, -xAng)+PI);
  while((int)angulozderecho == 225){
    Serial.println("Error de medida derecho, midiendo de nuevo");
    Wire.beginTransmission(MPU_Der);
    Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
    int error = Wire.endTransmission(false);
    if(error > 0){
      Serial.print("Error de conexion con el dispositivo izquierdo");
      Serial.print(" Codigo error: ");
      if(error == 1){
        Serial.println("Dato muy largo");
      }
      else if(error == 2){
        Serial.println("NACK  transmit of address ");
      }
      else if(error == 3){
        Serial.println("NACK on transmit of data");
      }
      else if(error == 4){
        Serial.println("other error ");
      }
    }
    Wire.requestFrom(MPU_Der,6,true); //A partir del 0x3B, se piden 6 registros
    AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
    AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
    AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)

    int xAng = map(AcX,minVal,maxVal,-90,90);
    int yAng = map(AcY,minVal,maxVal,-90,90);
    int zAng = map(AcZ,minVal,maxVal,-90,90);

    x= RAD_TO_DEG * (atan2(-yAng, -zAng)+PI);
    y= RAD_TO_DEG * (atan2(-xAng, -zAng)+PI);
    angulozderecho = RAD_TO_DEG * (atan2(-yAng, -xAng)+PI);
  }

  //Leer los valores del Acelerometro de la IMU
  Wire.beginTransmission(MPU_Izq);
  Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
  error = (int) Wire.endTransmission(false);
  if(error > 0){
    Serial.print("Error de conexion con el dispositivo derecho");
    Serial.print("Codigo error: ");
    if(error == 1){
      Serial.println("Dato muy largo");
    }
    else if(error == 2){
      Serial.println("NACK transmit of address");
    }
    else if(error == 3){
      Serial.println("NACK transmit of data");
    }
    else if(error == 4){
      Serial.println("other error ");
    }
  }
  Wire.requestFrom(MPU_Izq,6,true); //A partir del 0x3B, se piden 6 registros
  AcX=Wire.read()<<8|Wire.read(); //Cada valor ocupa 2 registros
  AcY=Wire.read()<<8|Wire.read();
  AcZ=Wire.read()<<8|Wire.read();

  xAng = map(AcX,minVal,maxVal,-90,90);
  yAng = map(AcY,minVal,maxVal,-90,90);
  zAng = map(AcZ,minVal,maxVal,-90,90);

  x= RAD_TO_DEG * (atan2(-yAng, -zAng)+PI);
  y= RAD_TO_DEG * (atan2(-xAng, -zAng)+PI);
  angulozizquierdo = RAD_TO_DEG * (atan2(-yAng, -xAng)+PI);
  while((int)angulozizquierdo == 225){
    Serial.println("Error de medida izquierdo, midiendo de nuevo");
    Wire.beginTransmission(MPU_Izq);
    Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
    error = (int) Wire.endTransmission(false);
    if(error > 0){
      Serial.print("Error de conexion con el dispositivo derecho");
      Serial.print("Codigo error: ");
      if(error == 1){
        Serial.println("Dato muy largo");
      }
      else if(error == 2){
        Serial.println("NACK transmit of address");
      }
      else if(error == 3){
        Serial.println("NACK transmit of data");
      }
      else if(error == 4){
        Serial.println("other error ");
      }
    }
    Wire.requestFrom(MPU_Izq,6,true); //A partir del 0x3B, se piden 6 registros
    AcX=Wire.read()<<8|Wire.read(); //Cada valor ocupa 2 registros
    AcY=Wire.read()<<8|Wire.read();
    AcZ=Wire.read()<<8|Wire.read();

    xAng = map(AcX,minVal,maxVal,-90,90);
    yAng = map(AcY,minVal,maxVal,-90,90);
    zAng = map(AcZ,minVal,maxVal,-90,90);

    x= RAD_TO_DEG * (atan2(-yAng, -zAng)+PI);
    y= RAD_TO_DEG * (atan2(-xAng, -zAng)+PI);
    angulozizquierdo = RAD_TO_DEG * (atan2(-yAng, -xAng)+PI);
  }

  if(imprimir){
    Serial.print("Angulo z izquierdo: ");
    Serial.print(angulozizquierdo);
    Serial.print("Angulo z derecho: ");
    Serial.println(angulozderecho);
  }
}
