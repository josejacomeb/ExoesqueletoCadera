/**
   Programa de Exoesqueleto de Cadera
   @author Romero Jackeline y Taco Marco
   Control para motores mediante Gyro MPU6050 y Encoders internos
 **/
/***<---CAMBIAR LA NUMERACION DE LOS PINES ANTES DE CORRER--->***/

#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"
const int MPU_Der = 0x69; // Direccion I2C motor enceradoderecho
const int MPU_Izq = 0x68; // Direccion I2C motor enceradoderecho,

const int minVal = 265;
const int maxVal = 402;
//Angulos para las distintas posiciones
int angulooscilacion = 15;
int anguloparado = 270;
int angulosentado = 0;

double x;
double y;
double angulozderecho = 0;
double angulozizquierdo = 0;

//Ratios de conversion
#define A_R 16384.0
#define G_R 131.0
//Conversion de radianes a grados 180/PI
#define RAD_A_DEG = 57.295779

//MPU-6050 da los valores en enteros de 16 bits
//Valores sin refinar
int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ; //Guardar las aceleraciones y los giros Derecha

//Angulos
float Acc[2];
float Gy[2];
float Angle[2];
bool TERMINAL = false;
int xAng = 0, yAng = 0, zAng = 0;
int error = 0;

int muestras = 20;

#define DEBUG //Descomentar para mostrar datos en el terminal

MPU6050 accelgyroizq(0x69), accelgyroder(0x68);

// Variables de calibración de MPU
//                              XA      YA      ZA      XG      YG      ZG
const int MPUOffsetsDer[6] = {143, 545, 772,  70,  29, -16};
const int MPUOffsetsIzq[6] = { -4094, -725, 1597, -62, -33, -1};


void setup() {
  Serial.begin(115200);
  Serial.println("Inicializando dispositivos I2C...");
  Wire.begin(); //Inicia como master
  TWBR = 24;
  delay(500);
  accelgyroizq.setSleepEnabled(false);
  delay(500);
  accelgyroder.setSleepEnabled(false);
    delay(500);
  // initialize device
  Serial.println("Initializing I2C devices...");
  accelgyroizq.initialize();
  Serial.println("Inicializando izquierda");
  accelgyroder.initialize();
    Serial.println("Inicializando derecha");
  // verify connection
  Serial.println("Testing device connections...");
  Serial.println(accelgyroder.testConnection() ? "MPU6050 izq connection successful" : "MPU6050 izq connection failed");
  Serial.println(accelgyroizq.testConnection() ? "MPU6050 der connection successful" : "MPU6050 der connection failed");

  //Configuración de Offsets
  accelgyroder.setXAccelOffset(MPUOffsetsDer[0]);
  accelgyroder.setYAccelOffset(MPUOffsetsDer[1]);
  accelgyroder.setZAccelOffset(MPUOffsetsDer[2]);
  accelgyroder.setXGyroOffset(MPUOffsetsDer[3]);
  accelgyroder.setYGyroOffset(MPUOffsetsDer[4]);
  accelgyroder.setZGyroOffset(MPUOffsetsDer[5]);

  accelgyroizq.setXAccelOffset(MPUOffsetsIzq[0]);
  accelgyroizq.setYAccelOffset(MPUOffsetsIzq[1]);
  accelgyroizq.setZAccelOffset(MPUOffsetsIzq[2]);
  accelgyroizq.setXGyroOffset(MPUOffsetsIzq[3]);
  accelgyroizq.setYGyroOffset(MPUOffsetsIzq[4]);
  accelgyroizq.setZGyroOffset(MPUOffsetsIzq[5]);

}

void loop() {
  if (Serial.available()) {
    char c = Serial.read();
    Serial.print("{senal,");
    Serial.print(c);
    Serial.print("}");
  }
  aceleracion(true);
  delay(10);
}

void aceleracion(bool imprimir) {

  float ax1 = 0, ay1 = 0, az1 = 0;
  for (int i = 0; i < muestras; i++) {
    accelgyroder.getMotion6(&AcX, &AcY, &AcZ, &GyX, &GyY, &GyZ);
    ax1 += AcX;
    ay1 += AcY;
    az1 += AcZ;
  }
  ax1 /= muestras;
  ay1 /= muestras;
  az1 /= muestras;
  xAng = map(ax1, minVal, maxVal, -90, 90);
  yAng = map(ay1, minVal, maxVal, -90, 90);
  zAng = map(az1, minVal, maxVal, -90, 90);

  x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
  y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
  angulozderecho = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
  while ((int)angulozizquierdo == 225) {
    ax1 = 0, ay1 = 0, az1 = 0;
    for (int i = 0; i < muestras; i++) {
      accelgyroder.getMotion6(&AcX, &AcY, &AcZ, &GyX, &GyY, &GyZ);
      ax1 += AcX;
      ay1 += AcY;
      az1 += AcZ;
    }
    ax1 /= muestras;
    ay1 /= muestras;
    az1 /= muestras;
    xAng = map(ax1, minVal, maxVal, -90, 90);
    yAng = map(ay1, minVal, maxVal, -90, 90);
    zAng = map(az1, minVal, maxVal, -90, 90);

    x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
    y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
    angulozderecho = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
  }
  ax1 = 0, ay1 = 0, az1 = 0;
  for (int i = 0; i < muestras; i++) {
    accelgyroizq.getMotion6(&AcX, &AcY, &AcZ, &GyX, &GyY, &GyZ);
    ax1 += AcX;
    ay1 += AcY;
    az1 += AcZ;
  }
  ax1 /= muestras;
  ay1 /= muestras;
  az1 /= muestras;

  xAng = map(ax1, minVal, maxVal, -90, 90);
  yAng = map(ay1, minVal, maxVal, -90, 90);
  zAng = map(az1, minVal, maxVal, -90, 90);

  x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
  y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
  angulozizquierdo = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
  while ((int)angulozizquierdo == 225) {
    ax1 = 0, ay1 = 0, az1 = 0;
    for (int i = 0; i < muestras; i++) {
      accelgyroizq.getMotion6(&AcX, &AcY, &AcZ, &GyX, &GyY, &GyZ);
      ax1 += AcX;
      ay1 += AcY;
      az1 += AcZ;
    }
    ax1 /= muestras;
    ay1 /= muestras;
    az1 /= muestras;

    xAng = map(ax1, minVal, maxVal, -90, 90);
    yAng = map(ay1, minVal, maxVal, -90, 90);
    zAng = map(az1, minVal, maxVal, -90, 90);

    x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
    y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
    angulozizquierdo = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
  }
  if (imprimir) {
    Serial.print("{ai,");   Serial.print(angulozizquierdo); Serial.print(","); Serial.print("ad,");  Serial.print(angulozderecho); Serial.println("}\0");
  }
}
