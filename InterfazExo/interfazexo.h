#ifndef INTERFAZEXO_H
#define INTERFAZEXO_H

#include <QWidget>

namespace Ui {
class InterfazExo;
}

class InterfazExo : public QWidget
{
    Q_OBJECT

public:
    explicit InterfazExo(QWidget *parent = 0);
    ~InterfazExo();

private:
    Ui::InterfazExo *ui;
};

#endif // INTERFAZEXO_H
