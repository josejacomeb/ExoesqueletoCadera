/*
 * File:   main.c
 * Author: josejacomeba
 *
 * Created on March 26, 2019, 2:41 PM
 */



// PIC18F2550 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1L
#pragma config PLLDIV = 1       // PLL Prescaler Selection bits (No prescale (4 MHz oscillator input drives PLL directly))
#pragma config CPUDIV = OSC1_PLL2// System Clock Postscaler Selection bits ([Primary Oscillator Src: /1][96 MHz PLL Src: /2])
#pragma config USBDIV = 1       // USB Clock Selection bit (used in Full-Speed USB mode only; UCFG:FSEN = 1) (USB clock source comes directly from the primary oscillator block with no postscale)

// CONFIG1H
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator (HS))
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOR = OFF         // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Reset Voltage bits (Minimum setting 2.05V)
#pragma config VREGEN = OFF     // USB Voltage Regulator Enable bit (USB voltage regulator disabled)

// CONFIG2H
#pragma config WDT = OFF         // Watchdog Timer Enable bit (WDT enabled)
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3H
#pragma config CCP2MX = ON      // CCP2 MUX bit (CCP2 input/output is multiplexed with RC1)
#pragma config PBADEN = ON      // PORTB A/D Enable bit (PORTB<4:0> pins are configured as analog input channels on Reset)
#pragma config LPT1OSC = OFF    // Low-Power Timer 1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config MCLRE = OFF      // MCLR Pin Enable bit (RE3 input pin enabled; MCLR pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = ON         // Single-Supply ICSP Enable bit (Single-Supply ICSP enabled)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit (Block 0 (000800-001FFFh) is not code-protected)
#pragma config CP1 = OFF        // Code Protection bit (Block 1 (002000-003FFFh) is not code-protected)
#pragma config CP2 = OFF        // Code Protection bit (Block 2 (004000-005FFFh) is not code-protected)
#pragma config CP3 = OFF        // Code Protection bit (Block 3 (006000-007FFFh) is not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot block (000000-0007FFh) is not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM is not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit (Block 0 (000800-001FFFh) is not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit (Block 1 (002000-003FFFh) is not write-protected)
#pragma config WRT2 = OFF       // Write Protection bit (Block 2 (004000-005FFFh) is not write-protected)
#pragma config WRT3 = OFF       // Write Protection bit (Block 3 (006000-007FFFh) is not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) are not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot block (000000-0007FFh) is not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM is not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit (Block 0 (000800-001FFFh) is not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit (Block 1 (002000-003FFFh) is not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit (Block 2 (004000-005FFFh) is not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit (Block 3 (006000-007FFFh) is not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot block (000000-0007FFh) is not protected from table reads executed in other blocks)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#define _XTAL_FREQ 20000000

#define USE_OR_MASKS
#include <xc.h>
#include "usart.h"
#include "adc.h"
#include "string.h"
#include <stdlib.h>

volatile unsigned char Rxdata;


unsigned char config = 0, spbrg = 0, baudconfig = 0, i = 0;
unsigned int ADCResult = 0;
float voltage = 0, vbateria = 0, vpiezo1 = 0, vpiezo2 = 2, vpiezo3 = 3, vpiezo4 = 0;
char antebateria = 'e', antepiezo1 = '0', antvpiezo2 = 0, antvpiezo3 = 0, antvpiezo4 = 0;
unsigned char channel = 0x00, config1 = 0x00, config2 = 0x00, config3 = 0x00, portconfig = 0x00;
char estadobateria = 'a', velocidadprototipo = 'b', anteriorbotones = '0', retrovelocidad = 'a';
const float activacion = 0.3; 
volatile char estadoemergencia = 'f';

void interrupt datoPuertoSerial() {

    if (PIR1bits.RCIF) { /* Test for TRUE condition of UART receive interrupt flag bit   */
        //---USART Reception ---
        Rxdata = ReadUSART();
        if (Rxdata == 'b') {
            putsUSART((char *) estadobateria);
        } else if (Rxdata == 'e') {
            putsUSART((char *) estadoemergencia);
        }
        PIR1bits.RCIF = 0; /* clearing USART receive flag   */
    }
    if (INTCONbits.INT0IF) {
        if (estadoemergencia == 'e') estadoemergencia = 'f';
        else estadoemergencia = 'e';
        WriteUSART(0x0D);
        putsUSART((char *) estadoemergencia);
        WriteUSART('\n');
        INTCONbits.INT0IF = 0;
    }
}

void delay_ms(int tiempo) {
    for (int j = 0; j <= tiempo; j += 20) {
        __delay_ms(20);
    }
}

float convertirADC() {
    ConvertADC();
    while (BusyADC());
    ADCResult = (unsigned int) ReadADC();
    voltage = (ADCResult * 5.0) / 1024; // convert ADC count into voltage
    return voltage;
}

void main(void) {

    CloseUSART(); //turn off usart if was previously on
    CloseADC();
    //--initialize adc---
    /**** ADC configured for:
     * FOSC/2 as conversion clock
     * Result is right justified
     * Aquisition time of 2 AD
     * Channel 1 for sampling
     * ADC interrupt on
     * ADC reference voltage from VDD & VSS
     */
    config1 = ADC_FOSC_16 | ADC_RIGHT_JUST | ADC_8_TAD;
    config2 = ADC_CH0 | ADC_INT_OFF | ADC_REF_VDD_VSS;
    portconfig = ADC_5ANA;

    OpenADC(config1, config2, portconfig);

    /*//---initialize the adc interrupt and enable them---
    ADC_INT_ENABLE();
     */
    //-----configure USART -----
    config = USART_TX_INT_OFF | USART_RX_INT_ON | USART_ASYNCH_MODE | USART_EIGHT_BIT |
            USART_CONT_RX | USART_BRGH_LOW;
    //-----SPBRG needs to be changed depending upon oscillator frequency-------
    spbrg = 31; // spbrg = Fosc/(16*bauds) - 1
    //At 8Mhz of oscillator frequency & baud rate of 2400.
    OpenUSART(config, spbrg);
    //API configures USART for desired parameters
    baudconfig = BAUD_8_BIT_RATE | BAUD_AUTO_OFF;
    baudUSART(baudconfig);
    //Procedimiento interrupciones 
    /*
        Turn ON interrupt on USART
        Clear Interrupt Flag
        Enable Peripheral Interrupts
        Enable Global Interrupts
     */
    ADC_INT_DISABLE();
    PIR1bits.RCIF = 0; /* Reset EUSART Receive Interrupt Flag                  */
    IPR1bits.RCIP = 0; /* Low priority for EUSART Receive Interrupt            */
    PIE1bits.RCIE = 1; /* RX interrupt Enable                                  */
    INTCONbits.INT0IE = 1; //Inicializar la interrupcion externa    
    INTCON2bits.INTEDG0 = 0; //Falling edge para la interrupcion 0
    INTCONbits.RBIF = 1; //Activar el cambio de puerto
    INTCONbits.INT0F = 0; //Limpiar la interrupción
    INTCON2bits.RBIP = 1; //Activar alta prioridad
    INTCONbits.PEIE = 1; /* Peripheral Interrupt Enable                          */
    TRISB = 0x01;
    TRISC0 = 0;
    TRISC1 = 0;
    TRISC2 = 0;
    PORTCbits.RC0 = 1;
    PORTCbits.RC1 = 1;
    PORTCbits.RC2 = 1;
    delay_ms(1000);
    PORTCbits.RC0 = 0;
    PORTCbits.RC1 = 0;
    PORTCbits.RC2 = 0;
    ei(); /* Global Interrupt Enable INTCONbits.GIE = 1;          */ //Configurar las interrupciones globales
    for (;;) {
        SetChanADC(ADC_CH0);
        delay_ms(100);
        vbateria = convertirADC(); //Minimo voltaje de la bateria 6V, 6.5V medio
        if (vbateria >= 3.6) estadobateria = 'a';
        else if (vbateria < 3.6 && vbateria >= 3.1) estadobateria = 'm';
        else if (vbateria < 3.1) estadobateria = 'b';
        switch (estadobateria) {
            case 'a': //Alto
                PORTCbits.RC0 = 1;
                PORTCbits.RC1 = 0;
                PORTCbits.RC2 = 0;
                break;
            case 'b': //Bajo
                PORTCbits.RC0 = 0;
                PORTCbits.RC1 = 0;
                PORTCbits.RC2 = 1;
                break;
            case 'm': //medio 
                PORTCbits.RC0 = 0;
                PORTCbits.RC1 = 1;
                PORTCbits.RC2 = 0;
                break;
            default:
                PORTCbits.RC0 = 0;
                PORTCbits.RC1 = 0;
                PORTCbits.RC2 = 0;
                break;
        }
        if (antebateria != '0' && antebateria != estadobateria) {
            while (BusyUSART());
            WriteUSART(0x0D);
            putsUSART((char *) estadobateria);
            WriteUSART('\n');
        }
        antebateria = estadobateria;
        if (estadoemergencia == 'f') {
            //Caminar
            SetChanADC(ADC_CH1);
            __delay_ms(20);
            vpiezo1 = convertirADC(); //Piezo Caminar
            if (vpiezo1 >= 1) velocidadprototipo = 'w';
            else if (vpiezo1 < 1 && vpiezo1 >= 0.3) velocidadprototipo = 'v';
            if (antepiezo1 != '0' && antepiezo1 != velocidadprototipo) {
                switch (velocidadprototipo) {
                    case 'v': //Bajo
                        PORTCbits.RC0 = 0;
                        PORTCbits.RC1 = 0;
                        PORTCbits.RC2 = 0;
                        delay_ms(300);
                        PORTCbits.RC0 = 1;
                        delay_ms(300);
                        PORTCbits.RC1 = 1;
                        delay_ms(300);
                        PORTCbits.RC2 = 0;
                        delay_ms(300);
                        break;
                    case 'w': //ALTO 
                        PORTCbits.RC0 = 0;
                        PORTCbits.RC1 = 0;
                        PORTCbits.RC2 = 0;
                        delay_ms(300);
                        PORTCbits.RC0 = 1;
                        delay_ms(300);
                        PORTCbits.RC1 = 1;
                        delay_ms(300);
                        PORTCbits.RC2 = 1;
                        delay_ms(300);
                        break;
                }
                PORTCbits.RC0 = 0;
                PORTCbits.RC1 = 0;
                PORTCbits.RC2 = 0;
                delay_ms(200);
                PORTCbits.RC0 = 1;
                PORTCbits.RC1 = 1;
                PORTCbits.RC2 = 1;
                delay_ms(200);
                while (BusyUSART());
                WriteUSART(0x0D);
                if (velocidadprototipo == 'w') putrsUSART((char *) "w");
                else if (velocidadprototipo == 'x') putrsUSART((char *) "x");
                else if (velocidadprototipo == 'v') putrsUSART((char *) "v");
                WriteUSART('\n');
            }
            antepiezo1 = velocidadprototipo;
            //Levantarse
            SetChanADC(ADC_CH2);
            __delay_ms(20);
            vpiezo2 = convertirADC();
            if (vpiezo2 >= activacion) {
                if (anteriorbotones != 'l') {
                    PORTCbits.RC0 = 0;
                    PORTCbits.RC1 = 0;
                    PORTCbits.RC2 = 0;
                    delay_ms(200);
                    PORTCbits.RC0 = 1;
                    PORTCbits.RC1 = 1;
                    PORTCbits.RC2 = 1;
                    delay_ms(200);
                    while (BusyUSART());
                    WriteUSART(0x0D);
                    putsUSART((char *) "l");
                    WriteUSART('\n');
                }
                anteriorbotones = 'l';
            }
            //Sentarse
            SetChanADC(ADC_CH3);
            __delay_ms(20);
            vpiezo3 = convertirADC();
            if (vpiezo3 >= activacion) {
                if (anteriorbotones != 's') {
                    PORTCbits.RC0 = 0;
                    PORTCbits.RC1 = 0;
                    PORTCbits.RC2 = 0;
                    delay_ms(200);
                    PORTCbits.RC0 = 1;
                    PORTCbits.RC1 = 1;
                    PORTCbits.RC2 = 1;
                    delay_ms(200);
                    while (BusyUSART());
                    WriteUSART(0x0D);
                    putsUSART((char *) "s");
                    WriteUSART('\n');
                }
                anteriorbotones = 's';
            }
            //Retro
            SetChanADC(ADC_CH4);
            __delay_ms(20);
            vpiezo4 = convertirADC();
            
            if (vpiezo4 >= 1) retrovelocidad = 't';
            else if (vpiezo4 < 1 && vpiezo4 >= 0.3) retrovelocidad = 'r';
            if (vpiezo4 >= activacion) {
                if (anteriorbotones != retrovelocidad) {
                    PORTCbits.RC0 = 0;
                    PORTCbits.RC1 = 0;
                    PORTCbits.RC2 = 0;
                    delay_ms(200);
                    PORTCbits.RC0 = 1;
                    PORTCbits.RC1 = 1;
                    PORTCbits.RC2 = 1;
                    delay_ms(200);
                    while (BusyUSART());
                    WriteUSART(0x0D);
                    if(retrovelocidad == 'r') putsUSART((char *) "r");
                    else putsUSART((char *) "t");
                    WriteUSART('\n');
                }
                anteriorbotones = retrovelocidad;
            }
        } else {
            PORTCbits.RC0 = 0;
            PORTCbits.RC1 = 0;
            PORTCbits.RC2 = 0;
            delay_ms(200);
            PORTCbits.RC0 = 0;
            PORTCbits.RC1 = 0;
            PORTCbits.RC2 = 1;
            delay_ms(200);
        }
        delay_ms(100);
    }
}
