/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *logoESPE;
    QLabel *label_3;
    QLabel *label_4;
    QSplitter *splitter;
    QLabel *label_5;
    QCustomPlot *widget;
    QPushButton *botonSerial;
    QCheckBox *cbSentarse;
    QCheckBox *cbLevantarse;
    QLabel *logoMCT;
    QSlider *horizontalSlider;
    QLabel *label;
    QCheckBox *cbRetro;
    QCheckBox *cbEmergencia;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1024, 768);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        logoESPE = new QLabel(centralWidget);
        logoESPE->setObjectName(QStringLiteral("logoESPE"));
        logoESPE->setGeometry(QRect(50, -10, 81, 71));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(200, 0, 591, 41));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(200, 30, 591, 41));
        splitter = new QSplitter(centralWidget);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setGeometry(QRect(60, 120, 491, 411));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(splitter->sizePolicy().hasHeightForWidth());
        splitter->setSizePolicy(sizePolicy);
        splitter->setOrientation(Qt::Vertical);
        label_5 = new QLabel(splitter);
        label_5->setObjectName(QStringLiteral("label_5"));
        splitter->addWidget(label_5);
        widget = new QCustomPlot(splitter);
        widget->setObjectName(QStringLiteral("widget"));
        splitter->addWidget(widget);
        botonSerial = new QPushButton(centralWidget);
        botonSerial->setObjectName(QStringLiteral("botonSerial"));
        botonSerial->setGeometry(QRect(680, 140, 131, 34));
        cbSentarse = new QCheckBox(centralWidget);
        cbSentarse->setObjectName(QStringLiteral("cbSentarse"));
        cbSentarse->setGeometry(QRect(670, 270, 87, 22));
        cbLevantarse = new QCheckBox(centralWidget);
        cbLevantarse->setObjectName(QStringLiteral("cbLevantarse"));
        cbLevantarse->setGeometry(QRect(670, 250, 91, 22));
        logoMCT = new QLabel(centralWidget);
        logoMCT->setObjectName(QStringLiteral("logoMCT"));
        logoMCT->setGeometry(QRect(800, -10, 81, 71));
        horizontalSlider = new QSlider(centralWidget);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setGeometry(QRect(670, 210, 160, 20));
        horizontalSlider->setMaximum(4);
        horizontalSlider->setOrientation(Qt::Horizontal);
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(860, 210, 57, 18));
        cbRetro = new QCheckBox(centralWidget);
        cbRetro->setObjectName(QStringLiteral("cbRetro"));
        cbRetro->setGeometry(QRect(670, 290, 87, 22));
        cbEmergencia = new QCheckBox(centralWidget);
        cbEmergencia->setObjectName(QStringLiteral("cbEmergencia"));
        cbEmergencia->setGeometry(QRect(670, 310, 101, 22));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1024, 30));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        logoESPE->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><img src=\":/imagenes/ESPE.jpeg\"/></p></body></html>", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:18pt; font-weight:600;\">Universidad de las Fuerzas Armadas ESPE </span></p></body></html>", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600;\">CONTROL EXOESQUELETO</span></p><p align=\"center\"><br/></p></body></html>", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "Gr\303\241fico Giroscopios", Q_NULLPTR));
        botonSerial->setText(QApplication::translate("MainWindow", "Conectar Serial", Q_NULLPTR));
        cbSentarse->setText(QApplication::translate("MainWindow", "Sentarse", Q_NULLPTR));
        cbLevantarse->setText(QApplication::translate("MainWindow", "Levantarse", Q_NULLPTR));
        logoMCT->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><img src=\":/imagenes/MCT.jpg\"/></p></body></html>", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Caminar", Q_NULLPTR));
        cbRetro->setText(QApplication::translate("MainWindow", "Retro", Q_NULLPTR));
        cbEmergencia->setText(QApplication::translate("MainWindow", "Emergencia", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
