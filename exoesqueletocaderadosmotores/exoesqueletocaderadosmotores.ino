/**
   Programa de Exoesqueleto de Cadera
   @author Romero Jackeline y Taco Marco
   Control para motores mediante dos Gyro MPU6050
 **/

#include "controlgyrosaccel.h"

//Angulos para las distintas posiciones
int angulooscilacion = 15;
int anguloparado = 270;
int angulosentado = 350;

bool TERMINAL = false;

#define DEBUG //Descomentar para mostrar datos en el terminal
#define MDCW  10 //Pin para habilitacion y sentido de giro contra sentido manecillas del reloj motor derecho
#define MDCCW 9 //Pin para habilitacion y sentido de giro contra sentido manecillas del reloj motor derecho
#define MDP 8
#define MDPWM 11  //Pin de control PWM
#define MICW 5 //Pin para habilitacion y sentido de giro manecillas reloj motor izquierdo
#define MICCW 6//Pin para habilitacion y sentido de giro contra sentido manecillas del reloj motor izquierdo
#define MIP 4 //Parada pin externo
#define MIPWM 7 //Pin de Control PWM

bool emergencia = false;

char caracter; //Para guardar lso datos del serial
bool velocidad = false;

bool parado = false, caminando = false, sentado = true;
bool enceradoizquierdo = false, enceradoderecho = false; //Variables para verificar el encerado izquierdo y derecho
bool paradoizquierdo = false, paradoderecho = false;     //Vaariables para verificar la posicion del rotor cuando se manda a pararse
bool caminadoizquierdo = false, caminadoderecho = false;     //Vaariables para verificar la posicion del rotor en la posicion de caminar

bool adelanteizquierda = false, adelantederecha = false;
bool pararizquierda = false, pararderecha = false;
int offset = 5;      //Offset de 3º para el control, es dificil que se pare exactamente es por eso del offset
int umbralguante = 200;

void encerar(); //Funcion para encerar los dos motores
void pararse(); //Funcion para que se pare el paciente
void caminar(); //Función para que el exoesquelete haga la funcion de caminar
void opciones(char caracter); //Funcion para asignar las opciones de acuerdo al puerto serial
int velocidadmotor = 120;
int valorsensor = 0, valorsensoranterior = 0;
int error = 0;
int estadoanterior = 0;

void setup() {
  Serial.begin(115200);
  Serial1.begin(9600);
  pinMode(MDCW,  OUTPUT);
  pinMode(MDCCW, OUTPUT);
  pinMode(MICW, OUTPUT);
  pinMode(MICCW, OUTPUT);
  pinMode(MIP, OUTPUT);
  digitalWrite(MIP, LOW);
  digitalWrite(MDP, LOW);
  digitalWrite(MDCW, LOW);
  digitalWrite(MDCCW, LOW);
  digitalWrite(MICW, LOW);
  digitalWrite(MICCW, LOW);
  inicializarMPU();
  delay(2000);
  /*Serial.println("Iniciando");
    Serial1.print('e');
    caracter = Serial1.read();
    while (caracter != 'e' && caracter != 'f') {
    Serial1.print('e');
    if (Serial1.available()) caracter = Serial1.read();
    }
    if (caracter == 'e') {
    Serial.println("Emergencia");
    emergencia = true;
    }
    else {
    emergencia = false;
    }
    Serial1.print('b');
    caracter = Serial1.read();
    while (caracter != 'a' && caracter != 'b' && caracter != 'm' ) {
    Serial1.print('b');
    }
    Serial.print("{bat,"); Serial.print(caracter); Serial.print("}\0");*/
}

void loop() {
  aceleracion(true);
  if (Serial.available()) {
    caracter = Serial.read();
    opciones(caracter);
  }
  if (Serial1.available()) {
    caracter = Serial1.read();
    opciones(caracter);
  }
  if (emergencia) {
    digitalWrite(MIP, HIGH);
    digitalWrite(MICW, LOW); //GIRA HACIA ADELANTE
    digitalWrite(MICCW, LOW);
    digitalWrite(MIPWM, LOW);
    //analogWrite(MIPWM, velocidadmotor);
    Serial.println("Parar derecha");
    digitalWrite(MDP, HIGH);
    digitalWrite(MDCW, LOW); //PARAR DERECHA
    digitalWrite(MDCCW, LOW);
    digitalWrite(MDPWM, LOW);
  }
}

void encerar() {
  Serial.print("{estado,Sentarse}");
  pararizquierda = false;
  pararderecha = false;
  enceradoizquierdo = false;
  enceradoderecho = false;
  aceleracion(true);
  Serial.println("*********Encerando*********");
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.println(angulozderecho);
  digitalWrite(MIP, LOW);
  digitalWrite(MDP, LOW);
  if (angulozizquierdo >= 0 && angulozizquierdo < 180 ) {
    if (angulosentado >= 0  && angulosentado <= 180) {
      if ((angulosentado + offset) >= angulozizquierdo && (angulosentado - offset) <= angulozizquierdo) {
        enceradoizquierdo = true;
      }
      else enceradoizquierdo = false;
    }
    else if (angulosentado > 180  && angulosentado <= 360) {
      if ((angulosentado + offset - 360) >= angulozizquierdo && (angulosentado - offset - 360) <= angulozizquierdo) {
        enceradoizquierdo = true;
      }
      else enceradoizquierdo = false;
    }
  }
  else if (angulozizquierdo >= 180 && angulozizquierdo <= 360) {
    if (angulosentado >= 0  && angulosentado <= 180) {
      if ((angulosentado + offset) >= (angulozizquierdo - 360) && (angulosentado - offset) <= (angulozizquierdo - 360)) {
        enceradoizquierdo = true;
      }
      else enceradoizquierdo = false;
    }
    else if (angulosentado > 180  && angulosentado <= 360) {
      if ((angulosentado + offset) >= angulozizquierdo && (angulosentado - offset) <= angulozizquierdo) {
        enceradoizquierdo = true;
      }
      else enceradoizquierdo = false;
    }
  }
  if (angulozderecho >= 0 && angulozderecho < 180 ) {
    if (angulosentado >= 0  && angulosentado <= 180) {
      if ((angulosentado + offset) >= angulozderecho && (angulosentado - offset) <= angulozderecho) {
        enceradoderecho = true;
      }
      else enceradoderecho = false;
    }
    else if (angulosentado > 180  && angulosentado <= 360) {
      if ((angulosentado + offset - 360) >= angulozderecho && (angulosentado - offset - 360) <= angulozderecho) {
        enceradoderecho = true;
      }
      else enceradoderecho = false;
    }
  }
  else if (angulozderecho >= 180 && angulozderecho <= 360) {
    if (angulosentado >= 0  && angulosentado <= 180) {
      if ((angulosentado + offset) >= (angulozderecho - 360) && (angulosentado - offset) <= (angulozderecho - 360)) {
        enceradoderecho = true;
      }
      else enceradoderecho = false;
    }
    else if (angulosentado > 180  && angulosentado <= 360) {
      if ((angulosentado + offset) >= angulozderecho && (angulosentado - offset) <= angulozderecho) {
        enceradoderecho = true;
      }
      else enceradoderecho = false;
    }
  }
  if (enceradoizquierdo) {
    Serial.println("Rotor izquierdo en sitio adecuado");
    pararizquierda = true;
  }
  else {
    if (angulozizquierdo > 180  && angulozizquierdo <= 360 ) {
      if (angulosentado > 180  && angulosentado <= 360 ) {
        if ((angulosentado - angulozizquierdo) < 0) {
          adelanteizquierda = false;
          Serial.println("Mover rotor izquierdo hacia atras");
        }
        else {
          adelanteizquierda = true;
          Serial.println("Mover rotor izquierdo hacia adelante");
        }
      }
      else if (angulosentado >= 0  && angulosentado <= 180 ) {
        if ((angulosentado + 360 - angulozizquierdo) > 0) {
          adelanteizquierda = false;
          Serial.println("Mover rotor izquierdo hacia atras");
        }
      }
    }
    else if (angulozizquierdo >= 0  && angulozizquierdo <= 180 ) {
      if (angulosentado > 180  && angulosentado <= 360 ) {
        adelanteizquierda = false;
        Serial.println("Mover rotor izquierdo hacia atras");
      }
      else if (angulosentado >= 0  && angulosentado <= 180 ) {
        if ((angulosentado - angulozizquierdo) < 0) {
          adelanteizquierda = false;
          Serial.println("Mover rotor izquierdo hacia atras");
        }
        else {
          adelanteizquierda = true;
          Serial.println("Mover rotor izquierdo hacia adelante");
        }
      }
    }
  }
  if (enceradoderecho) {
    Serial.println("Rotor derecha en sitio adecuado");
    pararderecha = true;
  }
  else {
    if (angulozderecho > 180  && angulozderecho <= 360 ) {
      if (angulosentado > 180  && angulosentado <= 360 ) {
        if ((angulosentado - angulozderecho) > 0) {
          adelantederecha = true;
          Serial.println("Mover rotor derecho hacia adelante");
          Serial.print("azd: [180-360]");
          Serial.println("as: [180-360]");
        }
        else {
          adelantederecha = false;
          Serial.println("Mover rotor derecho hacia atras");
          Serial.print("azd: [180-360]");
          Serial.println("as: [180-360]");
        }
      }
      else if (angulosentado >= 0  && angulosentado <= 180 ) {
        adelantederecha = true;
        Serial.println("Mover rotor derecho hacia atras");
        Serial.print("azd: [180-360]");
        Serial.println("as: [0-180]");
      }
    }
    else if (angulozderecho >= 0  && angulozderecho <= 180 ) {
      if (angulosentado > 180  & angulosentado <= 360 ) {
        adelantederecha = false;
        Serial.println("Mover rotor derecho hacia atras");
        Serial.print("azd: [0-180]");
        Serial.println("as: [180-360]");
      }
      else if (angulosentado >= 0  && angulosentado <= 180 ) {
        if ((angulosentado - angulozderecho) > 0) {
          adelantederecha = true;
          Serial.println("Mover rotor derecha hacia adelante");
          Serial.print("azd: [0-180]");
          Serial.println("as: [0-180]");
        }
        else {
          adelantederecha = false;
          Serial.println("Mover rotor derecha hacia atras");
        }
      }
    }
  }
  while (!enceradoizquierdo || !enceradoderecho) { //Mientras este en otra posicion los pines
    if (Serial.available()) {
      caracter = Serial.read();
      opciones(caracter);
    }
    if (Serial1.available()) {
      caracter = Serial1.read();
      opciones(caracter);
    }
    if (emergencia) break;
    aceleracion(true);
#ifdef DEBUG
    Serial.print("Anguloz izquierdo: ");
    Serial.print(angulozizquierdo);
    Serial.print("  Anguloz derecho: ");
    Serial.println(angulozderecho);
#endif
    if (pararizquierda) {
      Serial.println("Parar izquierda");
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);
      digitalWrite(MIP, HIGH);
      digitalWrite(MIPWM, velocidad);
      //analogWrite(MIPWM, 0);
    }
    else if (adelanteizquierda) {
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, LOW);
      digitalWrite(MIPWM, velocidad);
      //analogWrite(MIPWM, velocidadmotor);
    }
    else if (!adelanteizquierda) {
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, HIGH);
      digitalWrite(MIPWM, velocidad);
      //analogWrite(MIPWM, velocidadmotor);
    }
    if (pararderecha) {
      Serial.println("Parar derecha");
      digitalWrite(MDP, HIGH);
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
      digitalWrite(MDPWM, velocidad);
      //analogWrite(MDPWM, 0);
    }
    else if (adelantederecha) {
      digitalWrite(MDP, LOW);
      digitalWrite(MDCW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, LOW);
      digitalWrite(MDPWM, velocidad);
      //analogWrite(MDPWM, velocidadmotor);
    }
    else if (!adelantederecha) {
      digitalWrite(MDP, LOW);
      digitalWrite(MDCW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, HIGH);
      digitalWrite(MDPWM, velocidad);
      //analogWrite(MDPWM, velocidadmotor);
    }
    if (!enceradoizquierdo) {
      if (angulozizquierdo >= 0 && angulozizquierdo < 180 ) {
        if (angulosentado >= 0  && angulosentado <= 180) {
          if ((angulosentado + offset) >= angulozizquierdo && (angulosentado - offset) <= angulozizquierdo) {
            enceradoizquierdo = true;
          }
          else enceradoizquierdo = false;
        }
        else if (angulosentado > 180  && angulosentado <= 360) {
          if ((angulosentado + offset - 360) >= angulozizquierdo && (angulosentado - offset - 360) <= angulozizquierdo) {
            enceradoizquierdo = true;
          }
          else enceradoizquierdo = false;
        }
      }
      else if (angulozizquierdo >= 180 && angulozizquierdo <= 360) {
        if (angulosentado >= 0  && angulosentado <= 180) {
          if ((angulosentado + offset) >= (angulozizquierdo - 360) && (angulosentado - offset) <= (angulozizquierdo - 360)) {
            enceradoizquierdo = true;
          }
          else enceradoizquierdo = false;
        }
        else if (angulosentado > 180  && angulosentado <= 360) {
          if ((angulosentado + offset) >= angulozizquierdo && (angulosentado - offset) <= angulozizquierdo) {
            enceradoizquierdo = true;
          }
          else enceradoizquierdo = false;
        }
      }
    }
    if (!enceradoderecho) {
      if (angulozderecho >= 0 && angulozderecho < 180 ) {
        if (angulosentado >= 0  && angulosentado <= 180) {
          if ((angulosentado + offset) >= angulozderecho && (angulosentado - offset) <= angulozderecho) {
            enceradoderecho = true;
          }
          else enceradoderecho = false;
        }
        else if (angulosentado > 180  && angulosentado <= 360) {
          if ((angulosentado + offset - 360) >= angulozderecho && (angulosentado - offset - 360) <= angulozderecho) {
            enceradoderecho = true;
          }
          else enceradoderecho = false;
        }
      }
      else if (angulozderecho >= 180 && angulozderecho <= 360) {
        if (angulosentado >= 0  && angulosentado <= 180) {
          if ((angulosentado + offset) >= (angulozderecho - 360) && (angulosentado - offset) <= (angulozderecho - 360)) {
            enceradoderecho = true;
          }
          else enceradoderecho = false;
        }
        else if (angulosentado > 180  && angulosentado <= 360) {
          if ((angulosentado + offset) >= angulozderecho && (angulosentado - offset) <= angulozderecho) {
            enceradoderecho = true;
          }
          else enceradoderecho = false;
        }
      }
    }
    if (enceradoizquierdo) {
      pararizquierda = true;
      Serial.println("Parar izquierda");
    }
    if (enceradoderecho) {
      pararderecha = true;
      Serial.println("Parar derecha");
    }
    delay(10);
    Serial.print("Encerado derecho: ");
    Serial.print(enceradoderecho);
    Serial.print(" Encerado izquierdo: ");
    Serial.println(enceradoizquierdo);
    Serial.print(" && ");
    Serial.println(!enceradoizquierdo && !enceradoderecho);
  }
  digitalWrite(MDCW, LOW); //PARA LOS MOTORES
  digitalWrite(MDCCW, LOW);
  digitalWrite(MICW, LOW);
  digitalWrite(MICCW, LOW);
  digitalWrite(MIP, HIGH);
  digitalWrite(MDP, HIGH);
  Serial.println("*****Acabo de encerar****");
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.println(angulozderecho);
  sentado = true;
  delay(10);
}
void pararse() {
  Serial.print("{estado,Pararse}");
  caminando = false;
  pararizquierda = false;
  pararderecha = false;
  sentado = false;
  aceleracion(true);
  Serial.print("{info,*********Parada*********");
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.print(angulozderecho); Serial.print("}");

  if (angulozizquierdo < anguloparado + offset && angulozizquierdo > anguloparado - offset) {
    paradoizquierdo = true;
  }
  else paradoizquierdo = false;

  if (angulozderecho < anguloparado + offset && angulozderecho > anguloparado - offset) {
    paradoderecho = true;
  }
  else paradoderecho = false;

  if (paradoizquierdo) {
    Serial.println("Rotor izquierdo en sitio adecuado");
    pararizquierda = true;
  }
  else {
    if ((angulozizquierdo > 270 && angulozizquierdo <= 360) | (angulozizquierdo >= 0 && angulozizquierdo <= 90) ) {
      adelanteizquierda = false;
      Serial.println("{accionmi,Mover rotor izquierdo hacia atras}");
    }
    else if (angulozizquierdo > 90 && angulozizquierdo <= 270) {
      adelanteizquierda = true;
      Serial.println("{accionmi, Mover rotor izquierdo hacia adelante}");
    }
  }
  if (paradoderecho) {
    Serial.println("Rotor derecho en sitio adecuado");
    pararderecha = true;
  }
  else {
    if ((angulozderecho > 270 && angulozderecho <= 360) | (angulozderecho >= 0 && angulozderecho <= 90) ) {
      adelantederecha = false;
      Serial.println("{accionmd,Mover rotor derecho hacia atras}");
    }
    else if (angulozderecho > 90 && angulozderecho <= 270) {
      adelantederecha = true;
      Serial.println("{accionmd,Mover rotor derecho hacia adelante}");
    }
  }

  while (!paradoizquierdo || !paradoderecho) { //Mientras este en otra posicion los pines
    if (Serial.available()) {
      caracter = Serial.read();
      opciones(caracter);
    }
    if (Serial1.available()) {
      caracter = Serial1.read();
      opciones(caracter);
    }
    if (emergencia) break;
    aceleracion(true);
#ifdef DEBUG
    Serial.print("Anguloz izquierdo: ");
    Serial.print(angulozizquierdo);
    Serial.print("  Anguloz derecho: ");
    Serial.println(angulozderecho);
#endif
    if (pararizquierda) {
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);
      digitalWrite(MIP, HIGH);
      digitalWrite(MIPWM, velocidad);;
      delay(100);
    }
    else if (adelanteizquierda) {
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, LOW);
      digitalWrite(MIPWM, velocidad);
      //analogWrite(MIPWM, velocidadmotor);
    }
    else if (!adelanteizquierda) {
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, HIGH);
      digitalWrite(MIPWM, velocidad);
      //analogWrite(MIPWM, velocidadmotor);
    }
    if (pararderecha) {
      digitalWrite(MDP, HIGH);
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
      digitalWrite(MDPWM, velocidad);
      //analogWrite(MDPWM, 0);
    }
    else if (adelantederecha) {
      digitalWrite(MDP, LOW);
      digitalWrite(MDCW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, LOW);
      digitalWrite(MDPWM, velocidad);
      //analogWrite(MDPWM, velocidadmotor);
    }
    else if (!adelantederecha) {
      digitalWrite(MDP, LOW);
      digitalWrite(MDCW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, HIGH);
      digitalWrite(MDPWM, velocidad);
      //analogWrite(MDPWM, velocidadmotor);
    }
    if (!paradoizquierdo) {
      if (angulozizquierdo < anguloparado + offset && angulozizquierdo > anguloparado - offset) {
        paradoizquierdo = true;
      }
      else paradoizquierdo = false;
    }
    if (!paradoderecho) {
      if (angulozderecho < anguloparado + offset && angulozderecho > anguloparado - offset) {
        paradoderecho = true;
      }
      else paradoderecho = false;
    }
    if (paradoizquierdo) pararizquierda = true;
    if (paradoderecho) pararderecha = true;
    delay(10);
    Serial.print("Parado izquierdo: "); Serial.print(paradoizquierdo);     Serial.print("Parado derecho: "); Serial.println(paradoderecho);
  }
  Serial.println("{info,Finalizo la parada}");
  digitalWrite(MDCW, LOW); //PARA LOS MOTORES
  digitalWrite(MDCCW, LOW);
  digitalWrite(MICW, LOW);
  digitalWrite(MICCW, LOW);
  digitalWrite(MIP, HIGH);
  digitalWrite(MDP, HIGH);
  parado = true;
  delay(10);
}

void caminar(bool sentido) {
  caminando = true;
  Serial.println("*****Caminando*****");
  pararizquierda = false;
  pararderecha = false;
  parado = false;
  digitalWrite(MIP, LOW);
  bool adelante = true;
  if (sentido) adelante = true;
  else adelante = false;
  aceleracion(true);
#ifdef DEBUG
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.println(angulozderecho);
#endif
  int offset = 3;
  if (adelante) {
    adelanteizquierda != adelanteizquierda; //= true;
    adelantederecha != adelantederecha; //= false;
  }
  else {
    adelanteizquierda != adelanteizquierda; //= false;
    adelantederecha != adelantederecha; //= true;
  } //Comentado pies
  for (;;) { //Mientras este en otra posicion los pines
    if (sentido){
      Serial.print("{estado,Caminar");
      if(velocidad) Serial.print(" rapido}");
      else Serial.print(" lento}");    
    }
    else{
      Serial.print("{estado,Retro");
      if(velocidad) Serial.print(" rapido}");
      else Serial.print(" lento}");
    }
    if (Serial1.available()) {
      caracter = Serial1.read();
      opciones(caracter);
    }
    if (Serial.available()) {
      caracter = Serial.read();
      opciones(caracter);
    }
    if (emergencia) break;
    if (caminando == false) break; //Parar caminar
    aceleracion(true);
#ifdef DEBUG
    Serial.print("Anguloz izquierdo: ");
    Serial.print(angulozizquierdo);
    Serial.print("  Anguloz derecho: ");
    Serial.println(angulozderecho);
#endif
    if (pararizquierda) {
      digitalWrite(MIPWM, velocidad);
      Serial.println("Paro");
      digitalWrite(MIP, HIGH);
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);
    }
    else if (adelanteizquierda) {
      digitalWrite(MIPWM, velocidad);
      Serial.println("Adelante izquierda");
      digitalWrite(MICW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, LOW);
      digitalWrite(MIP, LOW);
    }
    else if (!adelanteizquierda) {
      digitalWrite(MIPWM, velocidad);
      Serial.println("Atras izquierda");
      digitalWrite(MIP, LOW);
      digitalWrite(MICW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, HIGH);
    }
    if (pararderecha) {
      digitalWrite(MDPWM, velocidad);;
      digitalWrite(MDP, HIGH);
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
    }
    else if (adelantederecha) {
      digitalWrite(MDP, LOW);
      digitalWrite(MDCW, HIGH); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, LOW);
      digitalWrite(MDPWM, velocidad);
    }
    else if (!adelantederecha) {
      digitalWrite(MDPWM, velocidad);
      digitalWrite(MDP, LOW);
      digitalWrite(MDCW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MDCCW, HIGH);
    }

    if (angulozizquierdo >= anguloparado + angulooscilacion && adelante) {
      caminadoizquierdo = true;
      pararizquierda = true;
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);
    }
    else if (angulozizquierdo <= anguloparado - angulooscilacion && !adelante) {
      caminadoizquierdo = true;
      pararizquierda = true;
      digitalWrite(MICW, LOW); //PARAR IZQUIERDA
      digitalWrite(MICCW, LOW);

    }
    if (angulozderecho >= anguloparado + angulooscilacion && !adelante) {
      caminadoderecho = true;
      pararderecha = true;
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
    }
    else if (angulozderecho <= anguloparado - angulooscilacion && adelante) {
      caminadoderecho = true;
      pararderecha = true;
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
    }
    if (caminadoizquierdo && caminadoderecho) {
      if (adelante) adelante = false;
      else adelante = true;
      caminadoizquierdo = false;
      caminadoderecho = false;
      pararderecha = false;
      pararizquierda = false;
    }
    if (adelante) {
      adelanteizquierda = true;
      adelantederecha = false;
    }
    else {
      adelanteizquierda = false;
      adelantederecha = true;
    }
    delay(10);
  }
  digitalWrite(MDCW, LOW); //PARA LOS MOTORES
  digitalWrite(MDCCW, LOW);
  digitalWrite(MICW, LOW);
  digitalWrite(MICCW, LOW);
  digitalWrite(MDP, HIGH);
  digitalWrite(MIP, HIGH);
  Serial.println("{info,*****Acabo de caminar*****");
  Serial.print("Anguloz izquierdo: ");
  Serial.print(angulozizquierdo);
  Serial.print("  Anguloz derecho: ");
  Serial.print(angulozderecho); Serial.print("}");

  delay(10);
}

void opciones(char caracter) {
  if (caracter == 'v' || caracter == 'w') {
    if (caracter == 'v') velocidad = false; //velocidad del motor baja
    else if (caracter == 'w') velocidad = true; //velocidad el motor alta
    if (sentado == true) {
      Serial.println("{alerta,Primero pare al prototipo por favor}");
    }
    else {
      digitalWrite(MIP, HIGH);
      digitalWrite(MICW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, LOW);
      digitalWrite(MIPWM, LOW);
      //analogWrite(MIPWM, velocidadmotor);
      Serial.println("Parar derecha");
      digitalWrite(MDP, HIGH);
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
      digitalWrite(MDPWM, LOW);
      delay(1000);
      caminar(true);
    }
  }
  else if (caracter == 'a') Serial.print("a"); //bateria alta
  else if (caracter == 'b') Serial.print("b"); //bateria baja
  else if (caracter == 'm') Serial.print("m"); //bateria media
  else if (caracter == 'l' && !emergencia) { //Levantarse
    if (sentado == true || caminando == true) {
      pararse();
    }
    else {
      Serial.println("{alerta,Primero siente al prototipo por favor}");
    }
  }
  else if (caracter == 's' && !emergencia) { //Sentarse
    if (caminando == true ) {
      Serial.println("{alerta,No se puede sentarse, pare el prototipo primero, for principal}");
    }
    else {
      encerar();
    }
  }
  else if ((caracter == 'r' || caracter == 't')  && !emergencia) { //Retro
    if (caracter == 'r') velocidad = false;
    else velocidad = true;
    if (sentado == true) {
      Serial.println("{alerta,Primero pare al prototipo por favor}");
    }
    else {
      digitalWrite(MIP, HIGH);
      digitalWrite(MICW, LOW); //GIRA HACIA ADELANTE
      digitalWrite(MICCW, LOW);
      digitalWrite(MIPWM, LOW);
      //analogWrite(MIPWM, velocidadmotor);
      Serial.println("Parar derecha");
      digitalWrite(MDP, HIGH);
      digitalWrite(MDCW, LOW); //PARAR DERECHA
      digitalWrite(MDCCW, LOW);
      digitalWrite(MDPWM, LOW);
      delay(1000);
      caminar(false);
    }
  }
  else if (caracter == 'e') { //Emergencia
    emergencia = true;
    digitalWrite(MIP, HIGH);
    digitalWrite(MICW, LOW); //GIRA HACIA ADELANTE
    digitalWrite(MICCW, LOW);
    digitalWrite(MIPWM, LOW);
    //analogWrite(MIPWM, velocidadmotor);
    Serial.println("Parar derecha");
    digitalWrite(MDP, HIGH);
    digitalWrite(MDCW, LOW); //PARAR DERECHA
    digitalWrite(MDCCW, LOW);
    digitalWrite(MDPWM, LOW);
  }
  else if (caracter == 'f') { // Fin emergencia
    emergencia = false;
  }
}
