//Basado en:
// I2C device class (I2Cdev) demonstration Arduino sketch for MPU6050 class using DMP (MotionApps v2.0)
// 6/21/2012 by Jeff Rowberg <jeff@rowberg.net>
// Updates should (hopefully) always be available at https://github.com/jrowberg/i2cdevlib
//

//Cabeceras I2C dev lib

#include "I2Cdev.h" //Funciones de MPU
#include "MPU6050_6Axis_MotionApps20.h" //Conversión del DPM
#include "Wire.h" //Comunicación I2C

MPU6050 mpuIzq(0x68), mpuDer(0x69); //Creación de Objetos MPU

//Los angulos serán medidos en Yaw, pitch, roll en grados

#define intDer 2 //Pines de interrupciones de MPU
#define intIzq 3

float angulozizquierdo = 0, angulozderecho = 0;

//Variables de control MPU

bool listoDMPDer = false, listoDMPIzq = false; //Si se iinicaliza el MPU
uint8_t mpuIntEstadoDer, mpuIntEstadoIzq; //Byte de estado de interrupciones MPU
uint8_t devStatusDer, devStatusIzq; //Estado de cada operación del dispositivo(0 éxito)
uint16_t packetSizeIzq, packetSizeDer; //Tamaño del paquete esperado(47 bytes)
uint16_t  fifoCountIzq, fifoCountDer; //Cuenta todos los bytes actualmente en el FIFO
uint8_t fifoBufferDer[64], fifoBufferIzq[64]; //FIFO storage buffer

  // Variables de calibración de MPU
//                              XA      YA      ZA      XG      YG      ZG
const int MPUOffsetsDer[6] = {-1713, 113, 1213,  61,  -35, - 6};
const int MPUOffsetsIzq[6] = {296, -1668, 1097, 237, 24, -12};

//Variables de orientacion

Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

//Interrupciones

volatile bool mpuInterruptDer = false, mpuInterruptIzq = false;     // Indica si la interrupcion paso a alto
// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

void dmpDataReadyDer() {
  mpuInterruptDer = true;
}
void dmpDataReadyIzq() {
  mpuInterruptIzq = true;
}

void inicializarMPU() {
  Wire.begin(); //Configuración I2C
  Wire.setClock(400000);
  Serial.println("***Inicializando dispositivos***");
  mpuDer.initialize(); //Inicializar MPUDerecha
  mpuIzq.initialize(); //Inicializar MPUIzquierda
  pinMode(intDer, INPUT); //Inicializar interrupciones como entradas
  pinMode(intIzq, INPUT);

  // verificar la conexión
  Serial.println(F("Probando las conexiones..."));
  Serial.println(mpuDer.testConnection() ? F("MPU6050 conexión exitosa") : F("MPU6050 no conectado"));
  Serial.println(mpuIzq.testConnection() ? F("MPU6050 conexión exitosa") : F("MPU6050 no conectado"));
  //Cargar y configurar the DMP
  Serial.println(F("Inicializando DMP..."));
  devStatusDer = mpuDer.dmpInitialize();
  devStatusIzq = mpuIzq.dmpInitialize();
  Serial.print("Inicialización Der: "); Serial.println(devStatusDer);
  Serial.print("Inicialización Izq: "); Serial.println(devStatusIzq);

  //Configuración de Offsets
  mpuDer.setXAccelOffset(MPUOffsetsDer[0]);
  mpuDer.setYAccelOffset(MPUOffsetsDer[1]);
  mpuDer.setZAccelOffset(MPUOffsetsDer[2]);
  mpuDer.setXGyroOffset(MPUOffsetsDer[3]);
  mpuDer.setYGyroOffset(MPUOffsetsDer[4]);
  mpuDer.setZGyroOffset(MPUOffsetsDer[5]);

  mpuIzq.setXAccelOffset(MPUOffsetsIzq[0]);
  mpuIzq.setYAccelOffset(MPUOffsetsIzq[1]);
  mpuIzq.setZAccelOffset(MPUOffsetsIzq[2]);
  mpuIzq.setXGyroOffset(MPUOffsetsIzq[3]);
  mpuIzq.setYGyroOffset(MPUOffsetsIzq[4]);
  mpuIzq.setZGyroOffset(MPUOffsetsIzq[5]);

  if (devStatusDer == 0) { //Si ha sido exitosa la configuracion del DPM
    Serial.println("***Habilitando DMP Derecho***");
    mpuDer.setDMPEnabled(true);

    // Habilitando detección de interrupciones
    Serial.print(F("Habilitando detección de interrupciones (Interrupción externa Arduino "));
    Serial.print(digitalPinToInterrupt(intDer));
    Serial.println(F(")..."));
    attachInterrupt(digitalPinToInterrupt(intDer), dmpDataReadyDer, RISING);
    mpuIntEstadoDer = mpuDer.getIntStatus();

    // set our DMP Ready flag so the main loop() function knows it's okay to use it
    Serial.println(F("DMP listo! Esperando a la primera interrupción..."));
    listoDMPDer = true;

    // Obtener el tamaño del paquete para una comparación posterior
    packetSizeDer = mpuDer.dmpGetFIFOPacketSize();
  }
  else {
    Serial.print(F("DMP Inicialización fallida (código "));
    Serial.print(devStatusDer);
    Serial.println(F(")"));


  }

  if (devStatusIzq == 0) { //Si ha sido exitosa la configuracion del DPM
    Serial.println("***Habilitando DMP Izquierdo***");
    mpuIzq.setDMPEnabled(true);

    // Habilitando detección de interrupciones
    Serial.print(F("Habilitando detección de interrupciones (Interrupción externa Arduino "));
    Serial.print(digitalPinToInterrupt(intIzq));
    Serial.println(F(")..."));
    attachInterrupt(digitalPinToInterrupt(intIzq), dmpDataReadyIzq, RISING);
    mpuIntEstadoIzq = mpuIzq.getIntStatus();

    // set our DMP Ready flag so the main loop() function knows it's okay to use it
    Serial.println(F("DMP listo! Esperando a la primera interrupción..."));
    listoDMPIzq = true;

    // Obtener el tamaño del paquete para una comparación posterior
    packetSizeIzq = mpuIzq.dmpGetFIFOPacketSize();
  }
  else {
    Serial.print(F("DMP Inicialización fallida (código "));
    Serial.print(devStatusIzq);
    Serial.println(F(")"));

  }
}

void calcularIzq() {
  mpuInterruptIzq = false;
  mpuIntEstadoIzq = mpuIzq.getIntStatus();
  fifoCountIzq = mpuIzq.getFIFOCount();
  if (!(fifoCountIzq) || (fifoCountIzq % packetSizeIzq)) {
    digitalWrite(13, LOW); // lets turn off the blinking light so we can see we are failing.
    mpuIzq.resetFIFO();// clear the buffer and start over
  } else {
    while (fifoCountIzq >= packetSizeIzq) {
      mpuIzq.getFIFOBytes(fifoBufferIzq, packetSizeIzq);
      fifoCountIzq -= packetSizeIzq;
    }
    mpuIzq.dmpGetQuaternion(&q, fifoBufferIzq);
    mpuIzq.dmpGetGravity(&gravity, &q);
    mpuIzq.dmpGetYawPitchRoll(ypr, &q, &gravity);
    angulozizquierdo = ypr[0] * 180 / M_PI + 180.0;
  }
}

void calcularDer() {
  mpuInterruptDer = false;
  mpuIntEstadoDer = mpuDer.getIntStatus();
  fifoCountDer = mpuDer.getFIFOCount();
  if (!(fifoCountDer) || (fifoCountDer % packetSizeDer)) {
    digitalWrite(13, LOW); // lets turn off the blinking light so we can see we are failing.
    mpuDer.resetFIFO();// clear the buffer and start over
  } else {
    while (fifoCountDer >= packetSizeDer) {
      mpuDer.getFIFOBytes(fifoBufferDer, packetSizeDer);
      fifoCountDer -= packetSizeDer;
    }
    mpuDer.dmpGetQuaternion(&q, fifoBufferDer);
    mpuDer.dmpGetGravity(&gravity, &q);
    mpuDer.dmpGetYawPitchRoll(ypr, &q, &gravity);
    angulozderecho = ypr[0] * 180 / M_PI + 180.0;
  }
}
